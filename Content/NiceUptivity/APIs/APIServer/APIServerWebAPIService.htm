﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="11" MadCap:lastHeight="8722" MadCap:lastWidth="1485">
    <head><title>API Server Web API Service</title>
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
        <meta name="description" content="Explains the architecture and how to interact with the [%=uWFO.NUShort%] API Serverusing the http web interface." />
    </head>
    <body>
        <h1>API Server Web API Service</h1>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot class="overview">Overview</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The <MadCap:variable name="uWFO.NULong" /> API&#160;Server can be utilized by any program that can communicate over HTTP. The WebAPI service accepts specially-formatted URLs that contain API data and translates the URL into a <MadCap:variable name="uWFO.NUShort" /> API function call which is then executed by the API Server. This allows the same API functionality as the API socket service provider for users more familiar with or applications only capable of making HTTP requests.</p>
                <p>For more information, see <MadCap:xref href="../APIOverview.htm">API Overview</MadCap:xref>.</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>
            <h2>General Architecture Overview</h2>
            <p>The <MadCap:variable name="uWFO.NULong" /> Web API Service runs on the <MadCap:variable name="uWFO.NUShort" /> server via a custom HTTP listener. Proper network firewall, security, and access measures must be in place to allow a custom or third-party application to access the server.</p>
            <p>An example of WebAPI service calls is shown in this diagram.</p>
            <div class="thumbnailImg">
                <img src="../Images/P_APIServerArchitecture.png" style="mc-thumbnail: popup;" />
            </div>
            <p>The WebAPI service is a built-in component on every <MadCap:variable name="uWFO.NUShort" /> system. The service has custom configuration options that must be set, and all calls to the service must follow the syntax that is accepted by the WebAPI.</p>
            <h3>TCP Port Settings</h3>
            <p>By default, the web service runs on port 2012 (2013 if SSL is used). These port settings can be modified if needed. See <MadCap:xref href="../../Application/Configuration/Services/APIServerSettingsReference.htm">API Server Settings</MadCap:xref> for more information on API Server configuration settings.</p>
            <p>To call the WebAPI properly, you must pass the API web service port number in the URL. The port number syntax is highlighted in the examples below.</p>
            <p>Non SSL-Enabled Connections Example:</p>
            <p class="code">http://discoverserveraddress:2012/webAPI.aspx</p>
            <p>SSL-Enabled Connections Example:</p>
            <p class="code">https://discoverserveraddress:2013/webAPI.aspx</p>
            <h3>WebAPI Call Syntax</h3>
            <p>To use the WebAPI functions, requests are passed to a specially-formatted URL that is accepted by the WebAPI service. An example of a WebAPI request call is below:</p>
            <p class="code">http://ccserveraddress:2012/webAPI.aspx?type=callupdate&amp;requestid=18213567&amp;deviceid=200&amp;user1=SpecialCall&amp;user2=10121968&amp;keepdays=3650</p>
            <p>Each request call must include the following:</p>
            <ul>
                <li class="topic"><b>Protocol Identifier</b> — This determines whether the call will be sent as plain text or will use SSL encryption. For plain text, set the value to HTTP. For SSL encryption, set the value to HTTPS.</li>
                <li class="topic"><b>Host Address</b> — This is the hostname or IP Address of the server running the <MadCap:variable name="uWFO.NUShort" /> API Service.</li>
                <li class="topic"><b>TCP Port</b> — This is the port number on which the API Service is listening for web requests.</li>
                <li class="topic"><b>API Page</b> — Calling this page allows an application to access any functions in the <MadCap:variable name="uWFO.NUShort" /> API, or allows an application access retrieve recorded audio files for any purpose, be it immediate playback or long-term storage. The value can be either <span class="fieldValue">/webAPI.php</span> or <span class="fieldValue">/webAPI.aspx</span>.</li>
                <li class="topic"><b>Function Type</b> — This indicates which API function you are passing to the API Service.</li>
                <li class="topic"><b>Function Parameters</b> — These are the required and optional parameters for the specified API function as defined for each function call type (call handling, status, and so forth). All API functions are available via the WebAPI method. For more information, see <MadCap:xref href="APIServerParamDefsAndDataTypeValues.htm">API Server Parameters and Data Types.</MadCap:xref></li>
            </ul>
            <p>To make a successful WebAPI call, the API function parameters must be passed to the WebAPI page. The parameters are passed using the question mark (?) character, and each parameter is delimited by the ampersand (&amp;) character. The desired value for each API parameter is set using an equal (=) sign, followed directly with the desired value for that parameter. Parameters are case-insensitive.</p>
            <p>You cannot use the same parameter more than once in a single WebAPI call as the parameters will be combined before passing to the <MadCap:variable name="uWFO.NUShort" /> API. For example:</p>
            <ul>
                <li class="topic">Your WebAPI call contains “deviceid=1234&amp;deviceid=5555”.</li>
                <li class="topic">The values are combined into the comma-separated list “deviceid=1234, 5555” before the <MadCap:variable name="uWFO.NUShort" /> API receives the command.</li>
                <li class="topic">The <MadCap:variable name="uWFO.NUShort" /> API treats this as a single value “1234,5555” and not two separate DeviceIDs.</li>
            </ul>
            <p>If your call contains duplicate parameters with different casing (for example, “deviceid=1234&amp;DEVICEID=5555”), the API returns an error message: “parameters must be unique but duplicates were found.” However, if you use multiple parameters with the same casing (for example, “deviceid=1234&amp;deviceid=5555”), the command simply will not return the correct results.</p>
            <h2>Examples of WebAPI Calls</h2>
            <h3>Update a Call Record</h3>
            <p>This example will update/mark the record with the listed data using the CALLUPDATE function. This function must be passed while the call is being actively recorded. For parameter requirements, see CALLUPDATE in <MadCap:xref href="APIServerCommands.htm">API Server Commands</MadCap:xref>.</p>
            <p>The WebAPI call should be formatted as follows:</p>
            <p class="code">http://ccserveraddress:2012/webAPI.aspx?type=callupdate&amp;requestid=18213567&amp;deviceid=200&amp;user9=SpecialCall&amp;user10=10121968&amp;keepdays=3650</p>
            <p>This example URL was constructed from the following components:</p>
            <ul>
                <li class="topic"><b>Protocol</b> — http</li>
                <li class="topic"><b>Host Address</b> — ccserveraddress</li>
                <li class="topic"><b>TCP Port</b> — 2012</li>
                <li class="topic"><b>API Page</b> — WebAPI.aspx</li>
                <li class="topic"><b>Function Type</b> — callupdate</li>
                <li class="topic"><b>Parameters</b>:
					<ul><li class="topic"><b>RequestID</b> — A unique identifier for the transaction generated by the user making the WebAPI call. The value must be an integer. In the example, the value is <span class="fieldValue">18213567</span>.</li><li class="topic"><b>DeviceID (required)</b> — The phone extension for the call being recorded. In the example, the value is <span class="fieldValue">200</span>.</li><li class="topic"><b>User9</b> — Custom-defined field (in this example, used to display a text phrase). In the example, the value is <span class="fieldValue">SpecialCall</span>.</li><li class="topic"><b>User10</b> — Custom-defined field (in this example, a transaction ID number). In the example, the value is <span class="fieldValue">10121968</span>.</li><li class="topic"><b>Keepdays</b> — The number of days this record will be maintained before being purged. In the example, the value is <span class="fieldValue">3650</span>.</li></ul></li>
            </ul>
            <h3>Export a Recorded Audio File</h3>
            <p>EXPORT is a function type unique to the WebAPI. This function allows <MadCap:variable name="uWFO.NUShort" /> to export recorded media from a call any time after recording has completed.</p>
            <p>The EXPORT function supports exporting audio only in the following formats: <MadCap:conditionalText MadCap:conditions="uWFO.DoNotPRINT">WAV (PCM), MP3, and VOX. You can also export the entire recording, including metadata, using the proprietary CAV format.</MadCap:conditionalText>WebM and PCM. PCM is only available if you are using a Third Party speech analytics solution.</p>
            <p>The EXPORT function has three required parameters: UNIQUEFIELD, UNIQUEID, and MEDIA_FORMAT. These parameters are used to identify the desired audio file and specify the export format.</p>
            <ul>
                <li class="topic"><b>UNIQUEFIELD</b> — Parameter name that contains the identifying information for the record. This can be any parameter inserted from a Call Handling API function, or any parameter returned from a DEVICELIST or DEVICESTATUS API function. The parameter used must contain data that is unique to an individual record. Any parameters that will not contain unique data (such as deviceid or devicealias) will return the most recent record that contains matching data. The possible values for UNIQUEFIELD are listed in <MadCap:xref href="APIServerParamDefsAndDataTypeValues.htm">API Server Parameters and Data Types</MadCap:xref>.</li>
                <li class="topic"><b>UNIQUEID</b> — Value in the UNIQUEFIELD parameter that uniquely identifies the record.</li>
                <li class="topic"><b>MEDIA_FORMAT</b> — File format of the exported audio file.</li>
            </ul>
            <p>Optional parameters ENCRYPT and PASSWORD may be used to encrypt an exported CAV file.</p>
            <ul>
                <li class="topic"><b>ENCRYPT</b> — Used to encrypt the exported CAV file, requires PASSWORD.</li>
                <li class="topic"><b>PASSWORD</b> — Password used to access the encrypted exported CAV.</li>
            </ul>
            <p>The WebAPI call should be formatted in the following manner:</p>
            <p class="code">http://ccserveraddress:2012/webAPI.aspx?type=export&amp;requestid=10482345&amp;uniquefield=user2&amp;uniqueid=10121968&amp;media_format=mp3&amp;encrypt=Y&amp;password=test</p>
            <p>This example URL was constructed from the following components:</p>
            <ul>
                <li class="topic"><b>Protocol</b> — http</li>
                <li class="topic"><b>Host Address</b> — ccserveraddress</li>
                <li class="topic"><b>TCP Port</b> — 2012</li>
                <li class="topic"><b>API Page</b> — WebAPI.aspx</li>
                <li class="topic"><b>Function Type</b> — export</li>
                <li class="topic"><b>Parameters</b>:
						<ul><li class="topic"><b>REQUESTID</b> — Unique identifier for the transaction generated by the user making the WebAPI call. The value must be an integer. In the example, the value is <span class="fieldValue">10482345</span>.</li><li class="topic"><b>UNIQUEFIELD</b> — Required parameter name that contains identifying information for the record. In the example, the value is the user-generated transaction ID <span class="fieldValue">user2</span>.</li><li class="topic"><b>UNIQUEID</b> — Value contained in the UNIQUEFIELD parameter for the record. In the example, the value is <span class="fieldValue">10121968</span>.</li><li class="topic"><b>MEDIA_FORMAT</b> — File format of exported audio file. In the example, the value is <span class="fieldValue">mp3</span>.</li><li class="topic"><b>ENCRYPT</b> — Optional parameter used to encrypt exported CAV file. In the example, the value is <span class="fieldValue">Y</span>.</li><li class="topic"><b>PASSWORD</b> — Required parameter if encryption is used. In the example, the value is <span class="fieldValue">test</span>.</li></ul></li>
            </ul>
            <p>Possible error returns are:</p>
            <ul>
                <li class="topic"><b>API_FILE_NOT_FOUND</b> — Appears if the specified file cannot be found.</li>
                <li class="topic"><b>API_INVALID_MESSAGE_FORMAT</b> — Appears if parameters are missing or the request is not formatted correctly.</li>
            </ul>
            <h3>Export Multiple Recorded Audio Files</h3>
            <p>COMBINEDEXPORT is unique to the WebAPI. This function type combines multiple audio files into a single file (post-recording) and exports the file in the specified format. It uses these values:</p>
            <ul>
                <li class="topic"><b>IDENT</b> — These are comma-separated call record IDs.</li>
                <li class="topic"><b>MEDIA_FORMAT</b> — File format of the exported audio file.</li>
            </ul>
            <p>The COMBINEDEXPORT call should be formatted in the following manner:</p>
            <p class="code">http://ccserveraddress:2012/webAPI.aspx?type=combinedexport&amp;requestid=10482345&amp;ident=10121967,10121968,10121969&amp;media_format=mp3</p>
            <p>This example URL was constructed from the following components:</p>
            <ul>
                <li class="topic"><b>Protocol</b> — http</li>
                <li class="topic"><b>Host Address</b> — ccserveraddress</li>
                <li class="topic"><b>TCP Port</b> — 2012</li>
                <li class="topic"><b>API Page</b> — WebAPI.aspx</li>
                <li class="topic"><b>Function Type</b> — combinedexport</li>
                <li class="topic"><b>Parameters</b>:
						<ul><li class="topic"><b>REQUESTID</b> — Unique identifier for the transaction generated by the user making the WebAPI call. The value must be an integer. In the example, the value is <span class="fieldValue">10482345</span>.</li><li class="topic"><b>IDENT</b> — Required parameter specifying the recordings to be combined. In the example, the values are the comma-separated call record IDs <span class="fieldValue">10121967,101219168,10121969</span>.</li><li class="topic"><b>MEDIA_FORMAT</b> — File format of exported audio file. In the example, the value is <span class="fieldValue">mp3</span>.</li></ul></li>
            </ul>
            <p>Possible error returns are:</p>
            <ul>
                <li class="topic"><b>API_FILE_NOT_FOUND</b> — Appears if the specified file cannot be found.</li>
                <li class="topic"><b>API_INVALID_MESSAGE_FORMAT</b> — Appears if parameters are missing or the request is not formatted correctly.</li>
            </ul>
            <h3>Export Multiple Recorded Audio Files as a ZIP File</h3>
            <p>MULTIEXPORT is unique to the WebAPI. This function type combines multiple audio files into a ZIP file (post-recording) and exports that file. It only uses these values:</p>
            <ul>
                <li class="topic"><b>IDENT</b> — These are comma-separated call record IDs.</li>
                <li class="topic"><b>MEDIA_FORMAT</b> — File format of the exported audio file.</li>
            </ul>
            <p>The MULTIEXPORT call should be formatted in the following manner:</p>
            <p class="code">http://ccserveraddress:2012/webAPI.aspx?type=multiexport&amp;requestid=10482345&amp;ident=10121967,10121968,10121969&amp;media_format=mp3</p>
            <p>This example URL was constructed from the following components:</p>
            <ul>
                <li class="topic"><b>Protocol</b> — http</li>
                <li class="topic"><b>Host Address</b> — ccserveraddress</li>
                <li class="topic"><b>TCP Port</b> — 2012</li>
                <li class="topic"><b>API Page</b> — WebAPI.aspx</li>
                <li class="topic"><b>Function Type</b> — multiexport</li>
                <li class="topic"><b>Parameters</b>:
				<ul><li class="topic"><b>REQUESTID</b> — Unique identifier for the transaction generated by the user making the WebAPI call. The value must be an integer. In the example, the value is <span class="fieldValue">10482345</span>.</li><li class="topic"><b>IDENT</b> — Required parameter specifying the recordings to be combined. In the example, the value is comma-separated call record IDs <span class="fieldValue">10121967,101219168,10121969</span>.</li></ul></li>
                <li class="topic"><b>MEDIA_FORMAT</b> — File format of exported audio file. In the example, the value is <span class="fieldValue">mp3</span>.</li>
            </ul>
            <p>Possible error returns are:</p>
            <ul>
                <li class="topic"><b>API_FILE_NOT_FOUND</b> — Appears if the specified file cannot be found.</li>
                <li class="topic"><b>API_INVALID_MESSAGE_FORMAT</b> — Appears if parameters are missing or the request is not formatted correctly.</li>
            </ul>
        </p>
        <h2>See Also</h2>
        <ul>
            <li><b><MadCap:xref href="APIServerCommands.htm">API Server Commands</MadCap:xref></b> — for information on how to send commands to the API Server</li>
            <li><b><MadCap:xref href="APIServerEventInterface.htm">API Server Event Interface</MadCap:xref></b> — for an introduction to writing XML&#160;API requests</li>
            <li><b><MadCap:xref href="APIServerParamDefsAndDataTypeValues.htm">API Server Parameters and Data Types</MadCap:xref></b> — for definitions of data you can include with API&#160;requests and the options and limitations of those values</li>
            <li><b><MadCap:xref href="APIServerResults.htm">API Server Command Results</MadCap:xref></b> — for an explanation of API&#160;responses, formatting, and how to interpret the output</li>
        </ul>
    </body>
</html>