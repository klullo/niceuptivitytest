﻿<?xml version="1.0" encoding="utf-8"?>
<html xml:lang="en-us" xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:conditions="uWFO.NotDone">
    <head>
        <meta name="description" content="Explains how to use the Retranscode tool to upgrade existing customers that are using legacy audio and video file formats to the WebM file format." />
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>Use the Retranscode Tool</h1>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Overview</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The retranscode console application is used to convert historical recording media in file formats such as .WAV and .VID to the .WEBM file format.</p>
                <p>Because .WEBM is the only file format that can be processed by <MadCap:variable name="uWFO.NUShort" /> version <MadCap:variable name="uWFO.Version" />, historical recording media from <MadCap:variable name="uWFO.NUShort" /> versions 17.3 and earlier must be retrancoded so that this media is compatible with version <MadCap:variable name="uWFO.Version" />.</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <h3>Before You Begin</h3>
        <p>Before retranscoding recording media, obtain a server that is separate from the production environment. 
				</p>
        <ul>
            <li class="topic">On the non-production server, install a RabbitMQ node and Transcoder 2.0</li>
            <li class="topic"> The RabbitMQ node must have access to the database</li>
        </ul>
        <p>See <a href="../RabbitMQ/RabbitMQOverview.htm">RabbitMQ&#160;Overview</a> and <a href="../../Application/Configuration/Services/TranscoderSettingsReference.htm">Transcoder Service Settings</a>.</p>
        <p>The retranscode tool is an executable file and the folder containing this executable can be obtained from the <MadCap:variable name="uWFO.NUShort" /> Installation team.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Available Commands</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The retranscode console application can perform three different functions:</p>
                <dl>
                    <dt>converttowebm</dt>
                    <dd>This command initiates a retranscode job, which converts historical recording audio and video media files to the WebM file format. You can add the <i>recordings</i> parameter to convert a specific set of files (records). Otherwise the command will initiate a retranscode job for all of the records in the database.</dd>
                    <dd>Examples:
					<p class="code">cc_retranscode converttowebm</p><p class="code">cc_retranscode converttowebm -recordings=1-100,104</p></dd>
                    <dt>retranscode</dt>
                    <dd>The <i>retranscode</i> command sends retranscode request messages to the Post Interaction Manager via RabbitMQ.  </dd>
                    <dd>The <i>recordings</i> parameter must be specified for this command, which is a range, and/or a comma separated list of database records (idents).</dd>
                    <dd>Examples:
				<p class="code">cc_retranscode retranscode -recordings=1-100
					</p><p class="code">cc_retranscode retranscode -recordings=1,4,5
</p><p class="code">cc_retranscode retranscode -recordings=1-100,104
				</p></dd>
                    <dt>estimatewebm</dt>
                    <dd>Using the <i>estimatewebm</i> command  will return an estimate of the time remaining for a specific set of records or an entire database to be transcoded. </dd>
                    <dd>You must configure the cc_retranscode.ini file to use this estimatewebm command (See <a href="#EstimationTool">Configure the Time Estimation Tool</a>).</dd>
                    <dd>You can add the <i>recordings</i> parameter to get a time estimate for a specific set of records (idents). Otherwise, the command will return a time estimate for all of the database's records.</dd>
                    <dd>Examples:
				<p class="code">cc_retranscode estimatewebm</p><p class="code">cc_retranscode estimatewebm -recordings=1-100,104</p></dd>
                </dl>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <br>
        </br>
        <h2>Retranscode Media</h2>
        <p>To convert historical recording media to the .WEBM file format, you must initiate the retranscode process on the non-production server on which you have installed a RabbitMQ node and an instance of Transcoder 2.0:</p>
        <ol class="stepList">
            <li>On the non-production server, open Command Prompt.<br></br></li>
            <li>To initiate the retranscode job, enter the following command:<p class="code">cc_retranscode converttowebm</p><br></br><p>Optional: If there is a specific range of files that you want to retranscode, enter the range of ident values for the recordings parameter. For example:</p><p class="code">cc_retranscode converttowebm -recordings=1-100,104</p></li>
        </ol>
        <br>
        </br>
        <h2><a name="EstimationTool"></a>Configure the Time Estimation Tool (Optional)</h2>
        <p>If you configure the cc_retranscode.ini file, you can use the estimatewebm command to receive an estimate of the amount of time required for a retranscode job to complete.</p>
        <p>The ability to estimate the time remaining for a retranscode job can be especially valuable if you are retranscoding media in larger databases. </p>
        <p>The  time estimation tool will not function if you do not configure the cc_retranscode.ini file according to the procedures below. </p>
        <div class="notification-note">
            <div class="notificationContent">
                <p>Configuring the time estimation tool is optional. The retranscode tool is not dependent on the configuration of the time estimation tool.</p>
            </div>
        </div>
        <p>To enable the time estimation tool, you must calculate two values (approximate average times to transcode an audio-only file and a video-only file) and then create an INI file that contains these two values.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Procedure</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <h3>Calculate time to transcode audio</h3>
                <p>The <b>time_to_transcode_audio</b> value is an estimation of the average time that it takes Transcoder 2.0 to retranscode an audio file from the historical recording media data set. Calculating this value enables the Transcoder 2.0 service to estimate the time remaining for a retranscode job if the transcoder receives an estimation request.</p>
                <p>To calculate the time_to_transcode_audio value:</p>
                <ol class="stepList">
                    <li>Open SQL&#160;Server Management Studio.</li>
                    <li>Collect a sample of 10 records of video-only files by using the following SQL&#160;query on the <MadCap:variable name="uWFO.NUShort"></MadCap:variable> database:			<p class="code">SELECT TOP 10 ident, filename, duration,filename_video FROM recordings WHERE filename LIKE '%.wav' AND filename_video =  AND duration &lt; 300 ORDER BY ident DESC</p></li>
                    <li>Look at the data for the records sample by entering the 10 ident values in the <b>WHERE</b>&#160;clause of the following query:
			<p class="code">SELECT ident, filename, duration,filename_video FROM recordings<br></br>WHERE ident in (<span class="installSpecific">&lt;COMMA&#160;SEPARATED&#160;IDENTS&gt;</span>)</p></li>
                    <li>Calculate the combined duration (in seconds) of the records sample by entering the 10 ident values in the <b>WHERE</b>&#160;clause of the following query:			<p class="code">SELECT sum(duration) FROM recordings<br></br>WHERE ident in (<span class="installSpecific">&lt;COMMA&#160;SEPARATED&#160;IDENTS&gt;</span>)</p></li>
                    <li>Next, retranscode the sample recording files to identify the time it takes to identify the total retranscode duration. 
				<ol><li class="topic">Open command prompt.</li><li class="topic">Retranscode the files. Enter the following command, adding the idents of the 10 records in the <b>recordings</b> parameter:
				<p class="code"> cc_retranscode converttowebm -recordings=<span class="installSpecific">&lt;COMMA&#160;SEPARATED&#160;IDENTS&gt;</span></p></li><li class="topic">Examine the Transcoder 2.0 logs and locate the start timestamp of the first file that was retranscoded.</li><li class="topic">Verify that the transcode job is complete by using the following query (If the query does not return any results, then all of the files were transcoded):<p class="code">SELECT ident, filename, duration, filename_video FROM recordings WHERE filename LIKE '%.wav' OR filename_video LIKE '%.vid' ORDER BY ident DESC</p></li><li class="topic">Examine the logs and locate the end timestamp of the last file that was retranscoded.</li><li class="topic">Calculate the time difference between the start and end timestamps to identify the total retranscode time.</li></ol></li>
                    <li>Calculate the time to transcode an audio-only file by dividing the total time of the retranscode job by the combined duration of the 10 sample records. 
				<p>For example, if our total retranscode time was 9 seconds and our combined recording time duration is 968 seconds, then our time to transcode audio value is 0.009297520, or 0.009.</p></li>
                </ol>
                <h3>calculate time to transcode video</h3>
                <p>Similar to the time_to_transcode_audio value, the <b>time_to_transcode_video</b> value is an estimation of the average time that it takes Transcoder 2.0 to retranscode a video file from the historical recording media data set. Calculating this value enables the Transcoder 2.0 service to provide an estimate of the time remaining for a retranscode job if the transcoder receives an estimation request.</p>
                <p>To calculate the time_to_transcode_video value:</p>
                <ol class="stepList">
                    <li>In SQL&#160;Server Management Studio, collect a sample of 10 records of video-only files by using the following SQL&#160;query on the <MadCap:variable name="uWFO.NUShort"></MadCap:variable> database:
			<p class="code">SELECT TOP 10 ident, filename,duration,filename_video FROM recordings WHERE filename= AND filename_video LIKE '%.vid' AND duration ORDER BY ident DESC</p></li>
                    <li>Look at the data for the records sample by entering the 10 ident values in the <b>WHERE</b>&#160;clause of the following query:
			<p class="code">SELECT ident, filename, duration,filename_video FROM recordings<br></br>WHERE ident in (<span class="installSpecific">&lt;COMMA&#160;SEPARATED&#160;IDENTS&gt;</span>)</p></li>
                    <li>Calculate the combined duration (in seconds) of the records sample by entering the 10 ident values in the <b>WHERE</b>&#160;clause of the following query:			<p class="code">SELECT sum(duration) FROM recordings<br></br>WHERE ident in (<span class="installSpecific">&lt;COMMA&#160;SEPARATED&#160;IDENTS&gt;</span>)</p></li>
                    <li>Next, retranscode the sample recording files to identify the time it takes to identify the total retranscode duration. 
				<ol><li class="topic">Open command prompt.</li><li class="topic">Retranscode the files. Enter the following command, adding the idents of the 10 records in the <b>recordings</b> parameter:
				<p class="code"> cc_retranscode converttowebm -recordings=<span class="installSpecific">&lt;COMMA&#160;SEPARATED&#160;IDENTS&gt;</span></p></li><li class="topic">Examine the Transcoder 2.0 logs and locate the start timestamp of the first file that was retranscoded.</li><li class="topic">Verify that the transcode job is complete by using the following query (If the query does not return any results, then all of the files were transcoded):<p class="code">SELECT ident, filename, duration, filename_video FROM recordings WHERE filename LIKE '%.wav' OR filename_video LIKE '%.vid' ORDER BY ident DESC</p></li><li class="topic">Examine the logs and locate the end timestamp of the last file that was retranscoded.</li><li class="topic">Calculate the time difference between the start and end timestamps to identify the total retranscode time.</li></ol></li>
                    <li>Calculate the time to transcode a video-only file by dividing the total time of the retranscode job by the combined duration of the 10 sample records. 
				</li>
                </ol>
                <h3>Create  ini file</h3>
                <p>Once you have calculated the time to transcode audio and time to transcode video values, you will need to create an INI file for the retranscode tool and enter those values in the INI file:</p>
                <ol class="stepList">
                    <li>Open a text editor and create a new file.<br></br></li>
                    <li>Add to the file the following properties and their corresponding values:				<p class="code">[server]<br></br>time_to_transcode_audio=<span class="installSpecific">&lt;TIME&#160;IN&#160;SECONDS&gt;</span><br></br>time_to_transcode_video=<span class="installSpecific">&lt;TIME&#160;IN&#160;SECONDS&gt;</span></p></li>
                    <li>Save the file:
				<ol><li class="topic">After clicking Save As, navigate to the following directory: (Local&#160;Disk)\Program Files (x86)\CallCopy.</li><li class="topic">Create a folder named <b>Retranscode_tool</b>.</li><li class="topic">In the Retranscode_tool folder, save the file as <b>cc_retranscode.ini</b>.</li></ol></li>
                </ol>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Use the Time Estimation Command</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Once the time estimation tool is configured, you can use the time estimation command during a retranscode job to receive an estimate of the time remaining for the retranscode job:</p>
                <ol class="stepList">
                    <li>On the non-production server, open Command Prompt.<br></br></li>
                    <li>To receive an estimation of the time remaining for the retranscode job, enter the following command:<p class="code">cc_retranscode estimatewebm</p><br /><p>Optional: You can add the recordings parameter to receive an estimate of the time remaining to retranscode a specific set of records:</p><p class="code">cc_retranscode estimatewebm -recordings=1-100,104</p></li>
                </ol>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <h2>See Also</h2>
        <ul>
            <li><b><MadCap:xref href="../../Recording/ScreenRecording/uWFOScreenRecordingOverview.htm"><MadCap:variable name="uWFO.Screen" /> Overview</MadCap:xref></b>
            </li>
        </ul>
    </body>
</html>