﻿<?xml version="1.0" encoding="utf-8"?>
<html xml:lang="en-us" MadCap:conditions="uWFO.Internal" xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <meta name="description" content="Explains how to configure a Secondary CXone site in a multi-site CXone integration." />
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>Configure a Secondary <MadCap:variable name="uWFO.CXoneShort" /> Site</h1>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Overview</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The <MadCap:variable name="uWFO.NUShort" />-<MadCap:variable name="uWFO.CXonePro" /> integration supports multiple media archives that are distributed across separate sites. (A site is a physical location where media is archived.)</p>
                <p>Multi-site support eliminates media file "round-tripping" for users that are playing back media in the same site that the media is stored. By eliminating media file round-tripping, less network bandwidth is used, which optimizes system performance.</p>
                <p>In a multi-site <MadCap:variable name="uWFO.CXoneShort" /> system, there is one Primary site and one or more Secondary sites. </p>
                <p>This topic explains how to set up a Secondary site in a multi-site <MadCap:variable name="uWFO.CXoneShort" /> system.</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <h3>Before you begin</h3>
        <p>The following requirements must be met before you can install and configure a Secondary <MadCap:variable name="uWFO.CXoneShort" /> site.</p>
        <ul>
            <li class="topic">A functional <MadCap:variable name="uWFO.CXoneShort" /> integration with <MadCap:variable name="uWFO.NUShort" /> must be available. This includes the Primary <MadCap:variable name="uWFO.CXoneShort" /> site.</li>
            <li class="topic">Each Secondary <MadCap:variable name="uWFO.CXoneShort" /> site must have all core <MadCap:variable name="uWFO.NUShort" /> services available, including recording services (a standalone call recorder, Web Socket Server (WSS), and IIS). The Secondary site must communicate with the central MS-SQL database through a unique site identifier.</li>
            <li class="topic">Do not install the SQL Data Adapter (SDA) service on a <MadCap:variable name="uWFO.CXoneShort" /> Secondary site. There should only ever be one instance of SDA running in a multi-site <MadCap:variable name="uWFO.CXoneShort" /> system, and that must be on the Primary <MadCap:variable name="uWFO.CXoneShort" /> server.</li>
        </ul>
        <h2>Step 1: Install <MadCap:variable name="uWFO.CXoneShort" /> Media Bridge</h2>
        <p>The <MadCap:variable name="uWFO.CXoneShort" /> Media Bridge (MB) service should already be available with the <MadCap:variable name="uWFO.NUShort" /> product installer. The first step to get a Secondary MB operational is to install it using the standard <b>-svcinst</b> command-line option.</p>
        <p>To install the Media Bridge service:</p>
        <ol class="stepList">
            <li>On the Secondary server, open a command prompt and navigate to the cx_wssMediaBridge folder in the Recorder directory.</li>
            <li>Install the service:<p class="code">cx_wssMediaBridge.exe -svcinst</p></li>
        </ol>
        <h2>Step 2: Configure IIS</h2>
        <p>Media playback from <MadCap:variable name="uWFO.CXoneShort" /> requires that IIS is configured to serve media files and HTTPS for the media URLs. The appropriate certificate must be in place and trusted by client workstations prior to installing <MadCap:variable name="uWFO.CXoneShort" />. </p>
        <div class="notification-note">
            <div class="notificationContent">
                <p><b>Note</b>: It is important that you correctly configure IIS  for media playback. <MadCap:variable name="uWFO.CXoneShort" /> will not play back media if it is not served via HTTPS, and the content headers are not correctly specified. The certificate used in IIS must also be trusted in the client to ensure that all browsers play media without any issues.</p>
            </div>
        </div>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Click for SSL&#160;certificate options:</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li class="topic">Customer deploys an in-house certificate authority (CA) to create a certificate for the Web Portal IIS server.<ul><li class="topic">This requires an installation of the root certificate for the CA on all agent workstations.</li></ul></li>
                    <li class="topic">Customer purchases an intranet certificate from a vendor.<ul><li class="topic">This requires an installation of the vendor's root certificate on all agent workstations.</li></ul></li>
                    <li class="topic">Using a self-signed certificate.<ul><li class="topic">Certificate can be generated from within IIS. This requires explicit "trusting" on all agent workstations.</li></ul></li>
                    <li class="topic">Using a publicly-trusted wildcard certificate<ul><li class="topic">This requires that the customer purchase a wildcard SSL certificate for the externally-facing web server, deploy that certificate to the Web Portal server, and ensure that the Web Portal server is accessible using the same root domain name as the externally-facing web server.</li><li class="topic">This does not require any changes to agent workstations.</li></ul></li>
                </ul>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>To configure IIS: </p>
        <ol class="stepList">
            <li>In IIS Manager,  create a new Virtual Directory called "cxone" in the main web site.<ul><li class="topic">The physical directory must be a location that is writeable by the cx_wssMediaBridge service.</li><li class="topic">If the IIS&#160;web server is on a different machine than cx_wssMediaBridge, the physical directory must be shared.</li></ul></li>
            <li>Right-click the main web site and select <b>Edit Bindings</b>.</li>
            <li>Add a binding for "https" using the appropriate server certificate.</li>
            <li>Select the "cxone" virtual directory and open <b>HTTP&#160;Response Headers</b>.</li>
            <li>Add the following header:<br /><p>&#160;</p><p>Name: Access-Control-Allow-Origin<br /></p><p>Value:&#160;*</p></li>
        </ol>
        <p>Once IIS&#160;is correctly configured, you can proceed to configuring the Media Bridge service.</p>
        <h2>Step 3:&#160;Configure the Media Bridge service</h2>
        <p>The quickest way to configure the Media Bridge service on a Secondary site is to copy the entire [wssmediabridge] section from the Primary site settings.ini file into the settings.ini on the Secondary site server. This ensures that <MadCap:variable name="uWFO.CXoneShort" />-specific settings such as tenant IDs and offline tokens are correct. Once that is done, some values need to be reconfigured.</p>
        <p>To configure the Media Bridge service:</p>
        <ol class="stepList">
            <li>Copy the <b>[wssmediabridge]</b> section of the settings.ini file.
				<ol><li>On the Primary server, open File Explorer. </li><li>Navigate to C:\Program Files (x86)\CallCopy.</li><li>Open the settings.ini file and copy the entire [wssmediabridge] section.</li></ol></li>
            <li>On the Secondary site server, open the settings.ini file and paste the <b>[wssmediabridge]</b> section in the file.
				<ol><li>On the Secondary server site, open File Explorer.</li><li>Navigate to C:\Program&#160;Files (x86)\CallCopy.</li><li>Open the settings.ini file.</li><li>Paste the [wssmediabridge] section.</li></ol></li>
            <li>
                If the following settings (highlighted in <span class="installSpecific">red</span>) do not exist in the settings.ini file on your Secondary server, copy and paste them  to the <b>[wssmediabridge]</b> section of the settings.ini file:<p class="code">[wssmediabridge]</p><p class="code">;</p><p class="code">; CXone Settings</p><p class="code">;</p><p class="code">; CXone tenant ID</p><p class="code">tenantId=</p><p class="code">; offline token used for Notification Service session management</p><p class="code">cxToken=</p><p class="code">; well-known URI of CXone "refresh token" request</p><p class="code">refreshTokenUri=https://na1.nice-incontact.com/public/asc/refresh</p><p class="code">; well-known URI of Notification Service</p><p class="code">notificationServiceUri=https://na1-ws.nice-incontact.com/ws/notifications/</p><p class="code">;</p><p class="code">; Media Settings</p><p class="code">;</p><p class="code">; directory/share for media files, accessible by web server</p><p class="code">mediaDirectory=D:\cx_temp\</p><p class="code">; base URI of media files (must be https)</p><p class="code">mediaBaseUri=https://localhost/cxone/</p><p class="code">; base URI for Web Socket Server endpoints</p><p class="code">wssBaseUri=ws://localhost:5650/</p><p class="code"><span class="installSpecific">; master/slave (if omitted, default is true)</span></p><p class="code"><span class="installSpecific">; mediaBridgeIsMaster=</span></p><p class="code"><span class="installSpecific">; when in slave mode, remote address of Media Bridge master (should NOT be localhost!)</span></p><p class="code"><span class="installSpecific">; when master with secure socket, must be the CN of the certificate (should NOT be localhost!)</span></p><p class="code"><span class="installSpecific">mediaBridgeMasterHost=</span></p><p class="code"><span class="installSpecific">; when in slave mode, the site "ident" of the slave</span></p><p class="code"><span class="installSpecific">mediaBridgeSiteIdent=</span></p><p class="code"><span class="installSpecific">; when in slave mode, use secure web sockets to communicate with master</span></p><p class="code"><span class="installSpecific">mediaBridgeMasterSecure=</span></p><p class="code">;</p><p class="code">; Advanced Settings</p><p class="code">;</p><p class="code">; port used to proxy messages between media bridge and Node.js proxy</p><p class="code">nsProxyServicePort=9998</p><p class="code"><span class="installSpecific">; pfx settings for secure web sockets (path relative to nsproxy directory)</span></p><p class="code"><span class="installSpecific">; nsProxyPfxPath=</span></p><p class="code"><span class="installSpecific">; nsProxyPfxPassword=</span></p><p class="code">; port used for external API (status, etc.)</p><p class="code">apiPort=9999</p><p class="code">; cache time (in minutes) for media files and URIs</p><p class="code">mediaCacheTimeout=15</p><p class="code">; container path to Node.js apps, or omit to use default at ./node/node.exe</p><p class="code">; nodePath=.\node\node.exe</p></li>
            <li>Configure the following settings: <div class="notification-note"><div class="notificationContent"><p><b>Important</b>: In the following settings, any uses of "<b>master</b>" refers to the <b>Primary</b> server. Any uses of "<b>slave</b>" refers to the <b>Secondary</b> server.</p></div></div><p><dl><dt>mediaDirectory</dt><dd>Specify the physical path of the temporary media files that are served by the "cxone" virtual directory. For example:<p class="code">; directory/share for media files, accessible by web server</p><p class="code">mediaDirectory=<span class="installSpecific">D:\cx_temp\</span></p></dd><dt>mediaBaseUri</dt><dd>Specify the URI to the "cxone" virtual directory on the Secondary IIS site. This setting must be the FQDN of the Secondary site and must be HTTPS. For example:<p class="code">; base URI of media files (must be https)</p><p class="code">mediaBaseUri=<span class="installSpecific">https://localhost/cxone/</span></p></dd><dt>mediaBridgeIsMaster</dt><dd>Uncomment this setting and specify <span class="fieldValue">false</span>. <p class="code">; master/slave (if omitted, default is true)</p><p class="code">mediaBridgeIsMaster=<span class="installSpecific">false</span></p></dd><dt>mediaBridgeMasterHost</dt><dd>Specify the FQDN of the Primary site Media Bridge. </dd><dd>An IP address is also a valid value if encrypted sockets are <i>not</i> being used.<p class="code">; when in slave mode, remote address of Media Bridge master (should NOT be localhost!)</p><p class="code">; when master with secure socket, must be the CN of the certificate (should NOT be localhost!)</p><p class="code">mediaBridgeMasterHost=<span class="installSpecific">exampledomain.lab</span></p></dd><dt>mediaBridgeSiteIdent</dt><dd>Specify the Ident of the Secondary site that you are configuring. For example:<p class="code">; when in slave mode, the site "ident" of the slave</p><p class="code">mediaBridgeSiteIdent=<span class="installSpecific">1</span></p></dd><dt>mediaBridgeMasterSecure</dt><dd>Indicates if the master site uses secure sockets. Specify <span class="fieldValue">false </span>if the master site does <i>not</i> use secure sockets.<p class="code">; when in slave mode, use secure web sockets to communicate with master</p><p class="code">mediaBridgeMasterSecure=<span class="installSpecific">false</span></p></dd></dl></p></li>
            <li>Ensure that the settings <b>nsProxyPfxPath</b> and <b>nsProxyPfxPassword</b> are commented out.</li>
        </ol>
        <div class="notification-note">
            <div class="notificationContent">
                <p><b>Reminder</b>:&#160;Do not install the SQL Data Adapter (SDA) service on a <MadCap:variable name="uWFO.CXoneShort" /> Secondary site. There should only ever be one instance of SDA running in a multi-site <MadCap:variable name="uWFO.CXoneShort" /> system, and that must be on the Primary <MadCap:variable name="uWFO.CXoneShort" /> server.</p>
            </div>
        </div>
        <h2>Step 4:&#160;Validate the Primary-Secondary Connection</h2>
        <p>When the Media Bridge  is configured on the Secondary site, starting the service should automatically initiate a connection to the Primary Media Bridge (assuming that the Primary Media Bridge service is running). The Media Bridge logs on the Primary and Secondary sites should indicate the successful connection, and the site identifier of the Secondary site.</p>
        <p>To validate the connection, perform the following steps in both the Primary server and the Secondary server:</p>
        <ol class="stepList">
            <li>Navigate to the Logs folder in the Program Files\callcopy directory.<br /></li>
            <li>
                Open the file cx_wssMediaBridge.log.</li>
            <li>View the log and look for messages indicating that the Secondary Media Bridge successfully connected with the Primary Media Bridge.</li>
        </ol>
        <p>After you have successfully established a connection between the Primary and Secondary <MadCap:variable name="uWFO.CXoneShort" /> sites, you can secure the connection. For more information, see <MadCap:xref href="SecureSiteConnections.htm">Secure the Primary-Secondary Connection</MadCap:xref>.</p>
        <h2>See Also</h2>
        <ul>
            <li>
                <MadCap:xref href="InstallingCXoneQMPro.htm" style="font-weight: bold;">Installing [%=uWFO.CXonePro%]</MadCap:xref>
            </li>
            <li>
                <MadCap:xref href="SecureSiteConnections.htm" style="font-weight: bold;">Secure the Primary-Secondary Connection</MadCap:xref>
            </li>
        </ul>
    </body>
</html>