﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:conditions="uWFO.Internal">
    <head><title>[%=uWFO.Speech%] Installation Overview</title>
        <meta name="description" content="Provides an overview of [%=uWFO.Speech%] installation." />
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <MadCap:snippetBlock src="../../../Resources/Snippets/TemplateSnippets/InstallTopics_header.flsnp" MadCap:conditions="MediaType.ScreenOnly" />
        <h1 class="preList"><MadCap:variable name="uWFO.Speech" /> Installation Overview</h1>
        <h2><MadCap:variable name="uWFO.Speech" /> Basics</h2>
        <p><MadCap:variable name="uWFO.Speech" /> is a separately-licensed application that is part of the <MadCap:variable name="uWFO.NULong" /> suite of products. It uses  phonetic search technology to process audio recorded by <MadCap:variable name="uWFO.NULong" />. After processing, customers can use tools in the <MadCap:variable name="uWFO.dWebPortal" /> to gather insights and analyze trends in their contact center.</p>
        <p><MadCap:variable name="uWFO.Speech" /> has been part of the <MadCap:variable name="uWFO.NULong" /> suite for some time, but until 17.2 was based on a different processing engine. As a result, the installation process has changed significantly. You should thoroughly read this and all linked topics prior to beginning an installation of <MadCap:variable name="uWFO.Speech" />.</p>
        <h2><MadCap:variable name="uWFO.Speech" /> Workflow</h2>
        <p>When an organization uses search grid-based <MadCap:variable name="uWFO.Speech" />, the platform uses the workflow described below. Note that this is significantly different from Aurix-based <MadCap:variable name="uWFO.Speech" />. Throughout the workflow, communication between services uses <span class="interface">RabbitMQ</span>. With search grid-based analytics in place:</p>
        <ul>
            <li class="topic"><MadCap:variable name="uWFO.NULong" /> records an interaction.</li>
            <li class="topic"><span class="interface">CTI Core</span> sends a message to the <span class="interface">Post Interaction Manager</span> (PIM) service that a new recording is complete.</li>
            <li class="topic"><span class="interface">PIM</span> informs the <span class="interface">Transcoder</span> to transcode the call. If the call was recorded under a schedule that has <a href="../../Recording/Scheduling/NewScheduleReference.htm#Speech">Speech Analytics</a> set to <span class="fieldValue">Yes</span>, <span class="interface">PIM</span> tells <span class="interface">Transcoder</span> to create a high-quality WAV copy of the recording.</li>
            <li class="topic">Once <span class="interface">Transcoder</span> has completed its work and informed <span class="interface">PIM</span>, <span class="interface">PIM</span> tells the <span class="interface">Analytics Manager</span> that a recording is available and ready for analytics processing.</li>
            <li class="topic"><span class="interface">Analytics Manager</span> handles communication with the search grid on the <MadCap:variable name="uWFO.Speech" /> server to ensure the file is analyzed and to store the resulting analytics data in the <MadCap:variable name="uWFO.NULong" /> database.</li>
            <li class="topic">Once the analytics work is complete, <span class="interface">Analytics Manager</span> causes the analytics WAV file to be deleted.</li>
        </ul>
        <h2>Installing <MadCap:variable name="uWFO.Speech" /></h2>
        <p class="preList">The <MadCap:variable name="uWFO.NUShort" /> Installation team will provide you with an install folder that includes:</p>
        <ul>
            <li class="topic"><MadCap:variable name="uWFO.Speech" /> installer (msi)</li>
            <li class="topic">Prerequisite software</li>
            <li class="topic">The appropriate search grid language pack for the implementation</li>
        </ul>
        <p>Licensing for the search grid is controlled separately. The <MadCap:variable name="uWFO.Speech" /> server must have access to the internet during the installation process in order to obtain this license. Once the installation is complete, there is no ongoing need for internet access for this server.</p>
        <p>Installation tasks should be performed in the order shown.</p>
        <table class="NiceUpTable">
            <col />
            <col />
            <thead>
                <tr>
                    <th>Step</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tr>
                <td>1</td>
                <td>Confirm that the <MadCap:variable name="uWFO.NUShort" /> installation is complete and the system is functioning as expected. This includes the integration to the ACD/PBX.</td>
            </tr>
            <tr>
                <td>2</td>
                <td>          Confirm that the customer has prepared a separate, dedicated server to host <MadCap:variable name="uWFO.Speech" />, that this server meets the proper specifications (see <MadCap:xref href="../../ServerAndSite/SpeechAnalytics_Requirements.htm">[%=uWFO.Speech%] Requirements</MadCap:xref>), and that  Windows PowerShell is enabled on the server.</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Confirm that the customer has created the appropriate local or AD accounts for the installation, and has assigned appropriate permissions to directories and shares. If the installation includes <MadCap:variable name="uWFO.NULong" />, these should already be in place (see <MadCap:xref href="../PrepAndPrereqs/AccountsPermissions.htm">Accounts and Permissions</MadCap:xref>).
                   </td>
            </tr>
            <tr>
                <td>4</td>
                <td>
                    <p>Confirm that the appropriate ports have been opened on the server and firewall to support the installation (see <MadCap:xref href="../../ServerAndSite/EnvironmentalRequirements.htm">Environmental Requirements</MadCap:xref>, <MadCap:xref href="../../ServerAndSite/SpeechAnalytics_Requirements.htm">[%=uWFO.Speech%] Requirements</MadCap:xref>, or both).</p>
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td>
                    <p>Confirm that the speech server is on the same network segment as the <MadCap:variable name="uWFO.NULong" /> server(s). If the server has multiple NICs, confirm that you have the IP address for the correct network interface.</p>
                </td>
            </tr>
            <tr>
                <td>6</td>
                <td>
                    <p>On the <MadCap:variable name="uWFO.Speech" /> server, create a shared directory and ensure that the <MadCap:variable name="uWFO.NULong" /> service account has read/write permissions to the directory. This is the location where <MadCap:variable name="uWFO.NULong" /> will place high-quality, WAV-formatted copies of recordings for processing by <MadCap:variable name="uWFO.Speech" />.</p>
                </td>
            </tr>
            <tr>
                <td>7</td>
                <td>
                    <p>Install prerequisite software. <MadCap:variable name="uWFO.Speech" /> requires the same prerequisite software as <MadCap:variable name="uWFO.NULong" /> with one addition (see <MadCap:xref href="../PrepAndPrereqs/InstallationPrereqs.htm">Installation Prerequisites</MadCap:xref>).</p>
                </td>
            </tr>
            <tr>
                <td>8</td>
                <td>
                    <MadCap:xref href="InstallSpeechAnalyticsServices.htm">Install the Search Grid</MadCap:xref>.
                </td>
            </tr>
            <tr>
                <td>9</td>
                <td>On the <MadCap:variable name="uWFO.NULong" /> server, register the <span class="interface">Analytics Manager</span> as a service and start it using either the <span class="interface">Service Manager</span> or Windows. See <MadCap:xref href="../uWFOInstallAndInitialConfig/RegisteringServicesInWindows.htm">Configure [%=uWFO.NUShort%] Services in Windows</MadCap:xref> and <MadCap:xref href="../../Application/Configuration/Services/AnalyticsManagerSettingsReference.htm">Analytics Manager Service Settings</MadCap:xref>.</td>
            </tr>
            <tr>
                <td>10</td>
                <td>Confirm that the <span class="interface">Post Interaction Manager</span> is configured and started. This should have been done in the <MadCap:variable name="uWFO.NULong" /> installation. See <MadCap:xref href="../../Application/Configuration/Services/PostInteractionManagerSettingsReference.htm">Post Interaction Manager Service Settings</MadCap:xref>.</td>
            </tr>
            <tr>
                <td>11</td>
                <td>Confirm that the <span class="interface">Transcoder</span> INI file has been configured correctly. This should have been done in the <MadCap:variable name="uWFO.NULong" /> installation. See <MadCap:xref href="../../Application/Configuration/Services/TranscoderSettingsReference.htm">Transcoder Service Settings</MadCap:xref>.</td>
            </tr>
            <tr>
                <td>12</td>
                <td>
                    <p>
                        <MadCap:xref href="ConfigureSpeechAnalytics.htm">Configure [%=uWFO.Speech%]</MadCap:xref> in the <MadCap:variable name="uWFO.dWebPortal" />.</p>
                    <p>OR</p>
                    <p>
                        <MadCap:xref href="ConfigMultiSiteSpeechAnalytics.htm">Configure Multi-Site Speech Analytics Environments</MadCap:xref>
                    </p>
                </td>
            </tr>
        </table>
        <h2>Upgrade Considerations</h2>
        <p>If you are installing <MadCap:variable name="uWFO.Speech"></MadCap:variable> as part of a customer upgrade, keep the following factors in mind:</p>
        <ul>
            <li class="topic">Neither search grid-based Speech Analytics is supported in <MadCap:variable name="uWFO.NULong" /> 17.1. Customers on 16.3 or earlier who want to upgrade and use <MadCap:variable name="uWFO.Speech" /> must go to 17.2 or later.</li>
            <li class="topic">Aurix-based Speech Analytics was only supported on servers running Windows 2008 Server. Search grid-based <MadCap:variable name="uWFO.Speech" /> is only supported on servers running Windows 2012 Server. Therefore, an operating system update will likely be required.  An upgrade of an existing speech analytics deployment is a good time to review current server hardware to determine whether a new server is needed to support current and anticipated speech processing functions.</li>
            <li class="topic">A distributed analytics model was introduced in v5.5. While the search grid can support multiple servers for different processing tasks, <MadCap:variable name="uWFO.Speech" /> does not support this in the 17.2 release. Adding this functionality is on the roadmap.</li>
            <li class="topic">Because the processing engine has changed completely, calls in queue for analytics processing on the old server cannot be carried over to the new server. The customer can choose to make a hard cutover (potentially losing speech processing for calls in the queue), or they can choose to let the queue run out before completing the upgrade. This would potentially cause loss of speech processing for calls recorded after the <MadCap:variable name="uWFO.NULong"></MadCap:variable> system is upgraded but before the <MadCap:variable name="uWFO.Speech"></MadCap:variable> upgrade is complete.</li>
            <li class="topic">Some customers used the <span class="interface">Transcoder</span> to create high-quality WAV files not only for analytics processing but for other purposes as well. This capability is not supported in the 18.1 release, but may return in a future release.</li>
            <li class="topic">The following <span class="interface">Transcoder</span> settings are no longer used and have been removed from the UI:
			<ul><li>Create Analytics</li><li>Analytics Keep Days</li><li>Analytic Storage Path</li><li>Look for Code</li></ul></li>
            <li class="topic">To determine whether an analytics WAV file is needed, the Post Interaction Manager  looks for a value of <span class="fieldValue">Yes</span> in the <span class="fieldName">Speech Analytics</span> field of the recording schedule. This setting is available on the <MadCap:xref href="../../Recording/Scheduling/NewScheduleReference.htm">New/Edit Schedule Page</MadCap:xref> when you create a new custom schedule. For other schedule types, you must first create the schedule and then edit it to access this setting.</li>
            <li class="topic">WAV files created for <MadCap:variable name="uWFO.Speech" /> are unencrypted.  If security of media files is a requirement, please contact <MadCap:variable name="uWFO.NUShort" /> Product for assistance.</li>
        </ul>
        <p MadCap:conditions="MediaType.ScreenOnly">&#160;</p>
        <table style="width: 100%;border-spacing: 0px 50px;border-bottom-style: solid;border-bottom-width: 2px;" MadCap:conditions="MediaType.ScreenOnly">
            <col />
            <col style="width: 314px;" />
            <tbody>
                <tr>
                    <td>
                        <p MadCap:conditions="MediaType.ScreenOnly"><a href="../../Application/SecurityAndEncryption/cc_cryptReference.htm">&lt;&lt; Previous section</a>
                        </p>
                        <p MadCap:conditions="MediaType.ScreenOnly">&#160;</p>
                    </td>
                    <td style="text-align: right;">
                        <p MadCap:conditions="MediaType.ScreenOnly"><a href="InstallSpeechAnalyticsServices.htm">Next Section &gt;&gt;</a>
                        </p>
                        <p MadCap:conditions="MediaType.ScreenOnly">&#160;</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p MadCap:conditions="MediaType.ScreenOnly">&#160;</p>
    </body>
</html>