﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:conditions="uWFO.Internal" MadCap:conditionTagExpression="include[uWFO.Internal] ">
    <head><title>RabbitMQ Overview</title>
        <meta name="description" content="Provides a conceptual overview of RabbitMQ in an [%=uWFO.NULong%] environment." />
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>RabbitMQ Overview</h1>
        <p>The <MadCap:variable name="uWFO.NUShort" /> suite uses a service bus model for designing and implementing communication between the mutually-interacting software applications that make up <MadCap:variable name="uWFO.NUShort" />.A third-party application, <span class="interface">RabbitMQ</span>, is used as the service bus for <MadCap:variable name="uWFO.NUShort" />. <span class="interface">RabbitMQ</span> is open source, easy to use, and runs on all major operating systems. </p>
        <p><span class="interface">RabbitMQ</span> <i>must</i> be installed on at least one server per customer site. This server, which includes one or more Erlang nodes, is sometimes referred to as a <span class="interface">RabbitMQ</span> broker. The following <MadCap:variable name="uWFO.NUShort" /> functionality is dependent on <span class="interface">RabbitMQ</span>:</p>
        <ul>
            <li class="topic"><b>Audio Recording</b> — communication between <b>CTI Core</b> and the <b>API</b> service requires <span class="interface">RabbitMQ</span></li>
            <li class="topic"><b>Screen Recording</b> — start/stop screen recording commands from <b>CTI&#160;Core</b> to the <b>Screen Capture Client</b> service requires <b>RabbitMQ</b></li>
            <li class="topic"><b>API&#160;Server</b> — start/stop blackout commands from <b>API&#160;Server</b> to the <b>Screen Capture Client</b> service requires <b>RabbitMQ</b></li>
            <li class="topic"><b>State Sourcing for API</b> — this  service monitors the state of an <MadCap:variable name="uWFO.NUShort" /> system and creates periodic "snapshots" that can be used by the <b>API Server</b> service on startup, allowing for quicker and better recovery from system failures</li>
            <li class="topic"><span class="interface">Post Interaction Manager</span> — this service handles processing of recordings after a call ends, and communicates with <span class="interface">CTI Core</span>, <span class="interface">Transcoder</span>, and <span class="interface">Analytics Manager</span> (if applicable) using <span class="interface">RabbitMQ</span></li>
        </ul>
        <h3 class="noCaps">Installation and Configuration</h3>
        <p>Tasks required to install <span class="interface">RabbitMQ</span> should be performed in the order shown below under <a href="#Related">Related Tasks</a>.</p>
        <h2>RabbitMQ Communication Model</h2>
        <p>The following is a high-level look at general <span class="interface">RabbitMQ</span> terminology and how it applies within <MadCap:variable name="uWFO.NUShort" />:</p>
        <ul>
            <li class="topic"><b>Producer</b> — an application that sends messages (for example, <span class="interface">CTI Core</span>)</li>
            <li class="topic"><b>Queue</b> — a buffer that stores messages awaiting a consumer</li>
            <li class="topic"><b>Consumer</b> — an application that receives messages (for example, <span class="interface">API Server</span>)</li>
            <li class="topic"><b>Exchange</b> — directs messages from producers to the appropriate queue</li>
        </ul>
        <p>For example, <span class="interface">CTI Core</span> (producer) generates a message that a call has started which should be recorded. The <span class="interface">RabbitMQ</span> exchange picks up the message and directs it to the appropriate queue for <span class="interface">API Server</span> (consumer) to receive. <span class="interface">API Server</span> can then issue the command to start recording.</p>
        <h2>System Design and Federation</h2>
        <p>When installing to a multi-server system, a best practice is to install a cluster of at least three <span class="interface">RabbitMQ</span> servers (or nodes) per customer site. This redundancy can protect against system failure due to <span class="interface">RabbitMQ</span> being unavailable. However, the nodes must be configured to correctly handle connectivity loss. This system design is not available for all implementations.</p>
        <p>The <MadCap:variable name="uWFO.NUShort"></MadCap:variable> Sales Engineer will determine which server(s) should host <span class="interface">RabbitMQ</span> when designing the system. <span class="interface">RabbitMQ</span> nodes are meant for LAN-based (not WAN-based) use, and should be configured to communicate only with other nodes at the same geographic location. </p>
        <div class="notification-note">
            <div class="notificationContent">
                <p><span class="interface">RabbitMQ</span> can be used by <MadCap:variable name="uWFO.NUShort"></MadCap:variable> even if it is installed on a non-<MadCap:variable name="uWFO.NUShort"></MadCap:variable> server. This may prove helpful in single-server <MadCap:variable name="uWFO.NUShort"></MadCap:variable> stand-alone  installations, where additional <span class="interface">RabbitMQ</span> nodes could potentially be installed on other customer servers that reside on the same LAN as the <MadCap:variable name="uWFO.NUShort"></MadCap:variable> server.</p>
            </div>
        </div>
        <h3 class="noCaps">Federation</h3>
        <p>If necessary, a <b>RabbitMQ</b> system can be configured to communicate between different geographic sites over a WAN. In a <b>federated</b> <b>RabbitMQ</b>&#160;system, <b>RabbitMQ</b> nodes have been configured to communicate with other <b>RabbitMQ</b> nodes in another geographic site across a WAN. </p>
        <p>A multi-site <MadCap:variable name="uWFO.NUShort" /> system will need to be federated if Screen Capture Clients on one site receive screen recording commands (and screen blackout commands) from <b>CTI&#160;Core</b> on a remote site (that is, on a separate LAN).</p>
        <div class="notification-note">
            <div class="notificationContent">
                <p>When communicating blackout commands in a federated system, the WAN's latency may result in sensitive data being captured. <b>For this reason, it is preferable to have an API server on each site.</b></p>
            </div>
        </div>
        <p>Federating <b>RabbitMQ</b> enables communication between remote <b>RabbitMQ</b> sites. To federate <b>RabbitMQ</b> in <MadCap:variable name="uWFO.NUShort" />, you must install federated exchanges.</p>
        <p>The federation exchange installer creates a federated exchange and an upstream exchange. An upstream exchange links a federated exchange to an exchange on a remote <b>RabbitMQ</b> broker. (A remote <b>RabbitMQ</b> broker is a broker that is hosted on a separate LAN.) When an upstream is created, the local <b>RabbitMQ</b> broker receives all of the messages published to the exchange on the remote <b>RabbitMQ</b> broker (via the federated exchange).</p>
        <p>An upstream only establishes one-way communication (from a remote broker to the local broker). For two-way communication, an upstream must be created on each of the two brokers that you want to connect.</p>
        <p>For <MadCap:variable name="uWFO.NUShort" />, you must establish two-way communication between the <MadCap:variable name="uWFO.NUShort" /> site that is hosting all of the required recording services and any dependent recording site. A dependent recording site is a site that must receive recording commands from a service on a remote recording site.</p>
        <p>The exchange that handles local messaging is named <b>recording</b>. The federated exchange is named <b>fed.recording</b>.</p>
        <div class="notification-note">
            <div class="notificationContent">
                <p>Not all multi-site implementations require federation. Whether the <MadCap:variable name="uWFO.NUShort" /> implementation is for audio recording or screen recording, a multi-site system does not need to be federated if each site hosts all of the required recording services.</p>
            </div>
        </div>
        <div class="notification-note">
            <div class="notificationContent">
                <p>Federated RabbitMQ instances should be secured by a VPN connection.</p>
            </div>
        </div>
        <p>For more information about configuring <b>RabbitMQ</b> for federation, see <a href="EnableFederation.htm">Enable Federation</a> and <a href="InstallFederationExchange.htm">Installing Federation Exchanges</a>.</p>
        <h2>RabbitMQ Web Portal</h2>
        <p>Once <span class="interface">RabbitMQ</span> has been installed and configured for an <MadCap:variable name="uWFO.NUShort" /> system, its web portal can be used to verify configuration and monitor messaging activity. The web address for the portal is always <span class="fieldValue">localhost:15672</span>.</p>
        <p>The following tabs provide useful information:</p>
        <ul>
            <li class="topic"><b>Overview</b> — provides a dashboard-style view of the <span class="interface">RabbitMQ</span> cluster
            <div class="thumbnailImg"><img src="Images/RabbitMQOverview1.png" style="mc-thumbnail: popup;" /></div></li>
            <li class="topic"><b>Exchanges</b> — displays all exchanges configured for the <span class="interface">RabbitMQ</span> cluster
			<div class="thumbnailImg"><img src="Images/InstallExchanges6.png" style="mc-thumbnail: popup;" /></div></li>
            <li class="topic"><b>Queues</b> — displays all messaging activity within the <span class="interface">RabbitMQ</span> cluster
                <div class="thumbnailImg"><img src="Images/RabbitMQOverview2.png" style="mc-thumbnail: popup;" /></div></li>
        </ul>
        <h2 MadCap:conditions="MediaType.ScreenOnly"><a name="Related"></a>Related Tasks</h2>
        <h3 MadCap:conditions="MediaType.PrintOnly"><a name="Related"></a>Related Tasks</h3>
        <ol>
            <li><b><a href="InstallingErlang.htm"><MadCap:xref href="InstallingErlang.htm">Install Erlang</MadCap:xref></a></b>
            </li>
            <li><b><a href="SettingEnvironmentVariable.htm"><MadCap:xref href="SettingEnvironmentVariable.htm">Set the Environment Variable</MadCap:xref></a></b>
            </li>
            <li><b><a href="InstallingRabbitMQ.htm"><MadCap:xref href="InstallingRabbitMQ.htm">Install RabbitMQ</MadCap:xref></a></b>
            </li>
            <li><b><a href="InstallingOpenSSL.htm"><MadCap:xref href="InstallingOpenSSL.htm">Install Open SSL</MadCap:xref></a></b>
            </li>
            <li><b><a href="ConfiguringRabbitMQNodes.htm"><MadCap:xref href="ConfiguringRabbitMQNodes.htm">Configure RabbitMQ Nodes</MadCap:xref></a></b>
            </li>
            <li><b><a hre="EnableFederation.htm">Enable Federation</a></b> — this task is required only in federated environments</li>
            <li><b><a href="InstallingExchanges.htm"><MadCap:xref href="InstallingExchanges.htm">Install Exchanges</MadCap:xref></a></b>
            </li>
            <li><b><a href="InstallFederationExhange.htm">Install Federation Exchanges</a></b> — this task is required only in federated environments</li>
            <li><b><a href="ConfiguringRabbitMQForSSL.htm"><MadCap:xref href="ConfiguringRabbitMQForSSL.htm">Configure RabbitMQ To Use SSL</MadCap:xref></a></b> — this task is required only in SSL environments</li>
            <li><b><a href="ConfiguringRabbitMQClusters.htm"><MadCap:xref href="ConfiguringRabbitMQClusters.htm">Configure RabbitMQ Clusters</MadCap:xref></a></b> — this task is not required if there is only one <span class="interface">RabbitMQ</span> server</li>
            <li><b><MadCap:xref href="ConfigureClusterAutoHeal.htm">Configure RabbitMQ Clusters for Partition Recovery</MadCap:xref></b> — this task is not required if there is only one <span class="interface">RabbitMQ</span> server</li>
        </ol>
        <h2 MadCap:conditions="MediaType.ScreenOnly"><a name="Related"></a>Related References</h2>
        <h3 MadCap:conditions="MediaType.PrintOnly"><a name="Related"></a>Related References</h3>
        <ul>
            <li><b><a href="../../QualityManagement/Evaluations/Calibration/PerformCalibrationEvaluation.htm"><MadCap:xref href="../../Application/Configuration/SettingsINIReference.htm">Settings INI File</MadCap:xref></a></b>
            </li>
        </ul>
    </body>
</html>