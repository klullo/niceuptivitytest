<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us">
    <head>
        <title>[%=uWFO.Screen%] Overview</title>
        <meta name="description" content="Provides a conceptual overview of [%=uWFO.Screen%]." />
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <MadCap:snippetBlock src="../../../Resources/Snippets/TemplateSnippets/InstallTopics_header.flsnp" MadCap:conditions="MediaType.ScreenInternalOnly" />
        <h1>
            <MadCap:variable name="uWFO.Screen" /> Overview</h1>
        <p>
            <MadCap:variable name="uWFO.Screen" /> gives you the ability to record agent workstation activity and to view agent desktops in near-real time. It is part of the base <MadCap:variable name="uWFO.NUShort" /> installation, but is enabled only if your organization has purchased the <MadCap:variable name="uWFO.Screen" /> feature.</p>
        <h2>Design Considerations</h2>
        <p>During the sales process, an <MadCap:variable name="uWFO.NUShort" /> sales engineer works with your organization to determine the best way to implement <MadCap:variable name="uWFO.Screen" />. The number of users or agents is a key issue. Growth in the number of agents affects performance and may necessitate changes to the implementation design. </p>
        <p MadCap:conditions="uWFO.Internal">Determining the number of users per screen recording server is complicated due to the different combinations of <MadCap:variable name="uWFO.NUShort" /> services that can be run from a single server. In a standalone configuration, where <b>Transcoder</b>, <b>Transcoder 2.0</b>, <b>Screen Capture API</b>, and <b>Web Socket Server</b> are the only <MadCap:variable name="uWFO.NUShort" /> services installed (except for the <b>Logger</b> service which is standard on every server), there can be a maximum of 750 simultaneous connections and 300 active concurrent screen recording or playback sessions.</p>
        <p MadCap:conditions="uWFO.Internal">The same server can host both voice and screen recording, or separate servers can be used. Separate <b>Transcoder</b> services must be installed for audio and video in either case. The system server topology is determined by the <MadCap:variable name="uWFO.NUShort" /> Sales Engineer and will be included with the installation workbook. To build a separate screen recording server, follow the normal installation procedures for <MadCap:variable name="uWFO.NUShort" /> up to the point of configuring services, and then proceed with the appropriate <a href="#Related">Related Tasks</a>.</p>
        <p>Interactions between <MadCap:variable name="uWFO.NUShort" /> components (for example, servers, web portals, and so forth), file servers, and archive devices can be secured. For more information, see <MadCap:xref href="../../Application/SecurityAndEncryption/SecurityOverview.htm">[%=uWFO.NUShort%] Security Overview</MadCap:xref>.</p>
        <p MadCap:conditions="uWFO.Internal">You should perform screen recording configuration tasks in the order shown below.</p>
        <h2>Services</h2>
        <p>All <MadCap:variable name="uWFO.NUShort" /> screen recording implementations utilize the following services:</p>
        <dl>
            <dt>CTI Core</dt>
            <dd>CTI&#160;Core communicates recording start and stop commands based on customer-defined recording schedules. Recording start and stop commands are sent to RabbitMQ and routed to the appropriate Screen Capture Client.</dd>
            <dt>RabbitMQ</dt>
            <dd>RabbitMQ routes recording commands from CTI&#160;Core to the appropriate Screen Capture Client. </dd>
            <dt>Screen Capture Client</dt>
            <dd>Screen Capture Client receives screen recording commands from CTI Core via RabbitMQ. During a screen recording session, Screen Capture Client captures and sends screen recording frames (images) to the Screen Capture API.</dd>
            <dt>Screen Capture API</dt>
            <dd>During a screen recording session, SCAPI receives screen capture frames from Screen Capture Client, collects all frames from a session, and when the session is finished, SCAPI sends the collection of frames to the final location  to be transcoded.</dd>
            <dt>Transcoder</dt>
            <dd>Transcoder converts the audio media from a recording   into a .WAV file.</dd>
            <dt>Transcoder 2.0</dt>
            <dd>Transcoder 2.0 transcodes the video media and then combines the transcoded audio and video files to create a WebM file.</dd>
        </dl>
        <h2>Schedule Considerations</h2>
        <p>When you configure schedules for screen recording, they must include the following settings:</p>
        <ul>
            <li class="topic">
                <b>Desktop Recording</b> — Set to <b>Yes</b></li>
            <li class="topic">
                <b>Screen Capture Wrap Length</b> — Configure to business requirements </li>
            <li class="topic">
                <b>Stop screen capture wrap on call start</b> — Configure to business requirements</li>
        </ul>
        <p>To fully support screen recording, you may need two schedules: one for audio recording only, and one for both audio and screen recording. For example, an organization wants to record 50 percent of all calls and ten percent of all calls must have both audio and screen. In this scenario, the organization needs an audio-only schedule to capture 40 percent of the calls and another schedule to capture audio and screen for 10 percent of calls.</p>
        <h2>Workstation Considerations</h2>
        <p> <MadCap:variable name="uWFO.Screen" /> uses client/server architecture, and the <MadCap:variable name="uWFO.Screen" /> client application must be installed and configured on every workstation to be recorded.</p>
        <p>The client can be installed manually or silently, with no user intervention required. <MadCap:conditionalText MadCap:conditions="uWFO.Internal">The customer is typically responsible for their user workstations. </MadCap:conditionalText>See <MadCap:xref href="InstallScreenClient.htm">Install the [%=uWFO.Screen%] Client</MadCap:xref> for more information.</p>
        <p>In most cases, you do not need to take special steps to ensure <MadCap:variable name="uWFO.NUShort" /> records the correct workstation for each user. By default, <MadCap:variable name="uWFO.NUShort" /> looks at the <span class="fieldName">System Username</span> (Windows login) and the System Domain (if the domain is required) in a user's profile, compares it to logged-in usernames reported by <MadCap:variable name="uWFO.Screen" /> clients, and matches the workstation to the user accordingly.</p>
        <h2>Upgrade Considerations</h2>
        <p>If you are upgrading from a previous version of <MadCap:variable name="uWFO.NUShort" />, screen recording will not be backward-compatible. Uninstall any earlier version of the <MadCap:variable name="uWFO.Screen" /> client before installing a new version. You can do this using Windows Programs &amp; Features functionality in the Control Panel. The software will be labeled "CallCopy ScreenCapture Client Software".</p>
        <p>Ensure that you have installed all of the prerequisite software before installing a new version of <MadCap:variable name="uWFO.NUShort" />. See the topics <MadCap:xref href="../../ServerAndSite/ServerRequirements.htm">Server Requirements</MadCap:xref> and <MadCap:xref href="../../ServerAndSite/PcRequirements.htm">PC&#160;Requirements</MadCap:xref>.</p>
        <p>Because .WEBM is the only file format that can be processed by <MadCap:variable name="uWFO.NUShort" /> version <MadCap:variable name="uWFO.Version" />, historical recording media (.WAV and .VID) from <MadCap:variable name="uWFO.NUShort" /> versions 17.3 and earlier must be retrancoded to the .WEBM format so that this media is compatible with version <MadCap:variable name="uWFO.Version" />. <MadCap:conditionalText MadCap:conditions="uWFO.Internal">The retranscode console application is used to convert historical recording media in file formats such as .WAV to the .WEBM file format. For more information about converting historical recording media, see <MadCap:xref href="../../Installation/UpgradeTools/UseRetranscodeTool.htm">Use the Retranscode Tool</MadCap:xref>. </MadCap:conditionalText></p>
        <p MadCap:conditions="MediaType.ScreenInternalOnly">&#160;</p>
        <table style="width: 100%;border-spacing: 0px 50px;border-bottom-style: solid;border-bottom-width: 2px;" MadCap:conditions="MediaType.ScreenInternalOnly">
            <col />
            <col style="width: 314px;" />
            <tbody>
                <tr>
                    <td>
                        <p MadCap:conditions="MediaType.ScreenInternalOnly">
                            <a href="../../Installation/uWFOInstallAndInitialConfig/BackUpTheSQLDatabase.htm">&lt;&lt; Previous section</a>
                        </p>
                        <p MadCap:conditions="MediaType.ScreenInternalOnly">&#160;</p>
                    </td>
                    <td style="text-align: right;">
                        <p MadCap:conditions="MediaType.ScreenInternalOnly">
                            <a href="../../Application/Configuration/Services/ScreenRecServerSettingsReference.htm">Next Section &gt;&gt;</a>
                        </p>
                        <p MadCap:conditions="MediaType.ScreenInternalOnly">&#160;</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p MadCap:conditions="MediaType.ScreenInternalOnly">&#160;</p>
        <h2 MadCap:conditions="MediaType.ScreenInternalOnly">
            <a name="Related">
            </a>Related Tasks</h2>
        <ul MadCap:conditions="MediaType.ScreenInternalOnly">
            <li class="topic">
                <b>
                    <MadCap:xref href="../../Application/Configuration/ConfiguringINIFiles.htm">Configure INI Files</MadCap:xref>
                </b>
            </li>
            <li class="topic">
                <b>
                    <MadCap:xref href="../../Application/Configuration/Services/ConfiguringServicesInNUWebPortal.htm">Configure Services in the [%=uWFO.dWebPortal%]</MadCap:xref>
                </b>
            </li>
            <li class="topic">
                <b>
                    <MadCap:xref href="../../Installation/uWFOInstallAndInitialConfig/RegisteringServicesInWindows.htm">Configure [%=uWFO.NUShort%] Services in Windows</MadCap:xref>
                </b>
            </li>
            <li class="topic">
                <b>
                    <MadCap:xref href="../../Installation/uWFOInstallAndInitialConfig/AddingServicesToSvcMgr.htm">Add [%=uWFO.NUShort%] Services to the Service Manager</MadCap:xref>
                </b>
            </li>
            <li class="topic">
                <b>
                    <MadCap:xref href="InstallScreenClient.htm">Install the [%=uWFO.Screen%] Client</MadCap:xref>
                </b>
            </li>
        </ul>
        <h2 MadCap:conditions="MediaType.ScreenInternalOnly">
            <a name="Related4">
            </a>Related References</h2>
        <ul MadCap:conditions="MediaType.ScreenInternalOnly">
            <li class="topic">
                <b>
                    <MadCap:xref href="../../Installation/uWFOInstallAndInitialConfig/ServiceConfigReference.htm">Windows Service Configuration Settings</MadCap:xref>
                </b>
            </li>
            <li class="topic">
                <b>
                    <MadCap:xref href="../../Application/Configuration/Services/ScreenRecServerSettingsReference.htm">[%=uWFO.Screen%] Server Settings</MadCap:xref>
                </b>
            </li>
            <li class="topic">
                <b>
                    <MadCap:xref href="SilentInstallRef.htm">Silent Installation Settings for [%=uWFO.NUShort%] Client Applications</MadCap:xref>
                </b>
            </li>
        </ul>
    </body>
</html>