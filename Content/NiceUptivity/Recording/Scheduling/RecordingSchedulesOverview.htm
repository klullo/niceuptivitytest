﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:lastBlockDepth="8" MadCap:lastHeight="3498" MadCap:lastWidth="746">
    <head><title>Recording Schedules Overview</title>
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
        <meta name="description" content="Provides a conceptual overview of recording schedules in [%=uWFO.NUShort%]." />
    </head>
    <body>
        <h1>Recording Schedules Overview</h1>
        <p>Administrators create schedules based on business rules to control which interactions are recorded. <MadCap:variable name="uWFO.NUShort" />  allows you to create several different types of schedules as described later in this topic.</p>
        <p>Most schedule types can be set up across any combination of call variables. All requests, including CTI messages and API requests related to call or agent information, are processed against existing schedules.</p>
        <p>If your implementation uses multiple instances of <span class="interface">CTI Core</span> (the service used in recording), you can choose to relate schedules to a specific core. If no schedules are related to a core, that core uses all schedules. If one or more schedules are related to a core, that core uses only the related schedules. For more information on this strategy, ask the <MadCap:variable name="uWFO.NUShort" /> Support team.</p>
        <p>Every recording-related event received by <MadCap:variable name="uWFO.NUShort" /> is compared against the business rules of all active schedules. An event may match one schedule, multiple schedules, or no schedules at all. When an event matches one or more schedules, an entry is logged for each individual match. </p>
        <p>Schedules are also used to specify various parameters of the recorded interactions. These include minimum and maximum recording length, sequence of archive actions, number of retention days, and so forth.</p>
        <p>You can assign a priority to each schedule so that, in the case of multiple matches, <MadCap:variable name="uWFO.NUShort" /> assigns the schedule with the preferred options. <MadCap:variable name="uWFO.Company" /> recommends that schedules be planned carefully and thoroughly before implementation, to make sure priorities are assigned correctly. Consider these examples:</p>
        <ul>
            <li class="topic">You create a schedule stating that audio and video for all calls will be recorded and retained for 30 days. You create another schedule for the Trust Group stating that only audio will be recorded for all calls, and that recordings will be retained for 365 days. Since keeping the Trust Group calls longer is more important, you should give that schedule a higher priority.</li>
            <li class="topic">John Smith (Agent 7258) has recently been coached for improper call handling, so you create a schedule to record all his calls between September 1 and September 15. A schedule is already in place for John’s ACD Group to record a randomized 20% of calls in September. The individual schedule for a specific purpose must be given a higher priority or it will be overridden by the ACD Group schedule.</li>
            <li class="topic">You create a schedule for the Support ACD Group to record audio for 100% of calls. You create another schedule for the same group to record video for 20% of calls. You create a schedule for the French ACD Gate to record audio and video for 100% of calls. Some agents are in both the Support ACD Group and the French ACD Gate. In order of highest to lowest, the schedules should be prioritized as: French ACD Gate, Support ACD Group video, Support ACD Group audio.</li>
        </ul>
        <p>Recording schedules can be edited, copied, or deleted. When a schedule is edited, the changes only apply to future calls recorded under that schedule. When a schedule is deleted, previously recorded calls are not affected.</p>
        <h2>Types of Recording Schedules</h2>
        <p>You can create the following types of recording schedules in <MadCap:variable name="uWFO.NUShort" />:</p>
        <ul>
            <li class="topic"><b>Record All Calls For An Agent During A Time Range</b> — This option allows you to quickly create a schedule for a single agent that will record their calls for a defined period. These schedules use default values for retention, recording length, and other parameters. If you want to record multiple agents, record only certain calls, or use non-default values for recording parameters, you must create a custom schedule.</li>
            <li class="topic"><b>Record The Next N Calls For An Agent</b> — This option allows you to quickly create a schedule for a single agent that will record a specific number of calls. The schedule can also be limited by date parameters. However, if the end date is reached before the specified number of calls, no additional calls will be recorded under the schedule. These schedules use default values for retention, recording length, and other parameters. If you want to record a specific number of calls for multiple agents or use non-default values for recording parameters, you must create a custom schedule.</li>
            <li class="topic"><b>Custom Schedules</b> — Custom schedules offer multiple parameters which can be used alone or in combination to create schedules that meet your organization's needs. All custom schedule types can incorporate random probability. In other words, when a call is delivered and the schedule is at or above its target percentage, the system generates a random number for the call, between 0 and 100. If the random number is equal to or less than the <span class="fieldName">Random Probability</span> value, the call is recorded. Otherwise, the call is skipped.</li>
            <li class="topic" MadCap:conditions="uWFO.DoNotPRINT"><b>Timed Schedules</b> — Timed schedules are used when there is no phone event to trigger recordings, such as with chat or email agents. Timed schedules let you record an agent's desktop for a specified time period, dividing the recordings incrementally according to your organizational needs. For example, recording could be scheduled from 8:00 AM to 5:00 PM, and each record could last 15 minutes. The desktop will be recorded provided the workstation is powered on and the <MadCap:variable name="uWFO.Screen" /> client software is running, whether the agent is actively using the workstation or not. This feature is licensed separately from voice and screen recording. Consult your <MadCap:variable name="uWFO.NUShort" /> representative for additional information.<div class="notification-note"><div class="notificationContent"><p>This feature will not work properly if you use the <span class="interface">Workstations List</span>. Unique usernames are required. Timed schedules can be edited and deleted, but not copied.</p></div></div></li>
        </ul>
        <h2><a name="Related"></a>Related Tasks</h2>
        <ul>
            <li><b><MadCap:xref href="CreateNumberCallsSchedules.htm">Create Recording Schedules Based on Number of Calls</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="CreateTimePeriodSchedules.htm">Create Recording Schedules for a Period of Time</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="CreateCustomSchedules.htm">Create Custom Recording Schedules</MadCap:xref></b>
            </li>
            <li MadCap:conditions="uWFO.DoNotPRINT"><b><MadCap:xref href="CreateTimedSchedules.htm">Create Timed Recording Schedules</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="FindSchedules.htm">Find Recording Schedules</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="EditSchedules.htm">Edit Recording Schedules</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="CopySchedules.htm">Copy Recording Schedules</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="DeleteSchedules.htm">Delete Recording Schedules</MadCap:xref></b>
            </li>
            <li MadCap:conditions="uWFO.DoNotPRINT"><b><MadCap:xref href="FindTimedSchedules.htm">Find Timed Recording Schedules</MadCap:xref></b>
            </li>
            <li MadCap:conditions="uWFO.DoNotPRINT"><b><MadCap:xref href="EditTimedSchedules.htm">Edit Timed Recording Schedules</MadCap:xref></b>
            </li>
            <li MadCap:conditions="uWFO.DoNotPRINT"><b><MadCap:xref href="DeleteTimedSchedules.htm">Delete Timed Recording Schedules</MadCap:xref></b>
            </li>
        </ul>
        <h2><a name="Related2"></a>Related References</h2>
        <ul>
            <li><b><MadCap:xref href="NewScheduleReference.htm">New/Edit Schedule Page</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="ScheduleQueryReference.htm">Schedule Query Page</MadCap:xref></b>
            </li>
            <li MadCap:conditions="uWFO.DoNotPRINT"><b><MadCap:xref href="TimedScheduleReference.htm">Timed Schedules Page</MadCap:xref></b>
            </li>
            <li MadCap:conditions="uWFO.DoNotPRINT"><b><MadCap:xref href="TimedScheduleListReference.htm">Timed Schedules List</MadCap:xref></b>
            </li>
        </ul>
    </body>
</html>