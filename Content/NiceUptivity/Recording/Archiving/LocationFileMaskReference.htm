﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:lastBlockDepth="5" MadCap:lastHeight="2882" MadCap:lastWidth="917">
    <head>
        <title>Location File Masks</title>
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
        <meta name="description" content="Provides a reference for the fields available to create custom filename masks for a variety of purposes in [%=uWFO.NULong%]." />
    </head>
    <body>
        <h1>Location File Masks</h1>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Overview</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>
                    <MadCap:variable name="uWFO.NUShort" /> allows you to create location file masks: custom UNC paths, file types, and file names to be used in saving, exporting, and archiving recording files.</p>
                <p>This topic explains the variables that can be used in location file masks.</p>
                <p>For more information, see <MadCap:xref href="NewArchiveActionReference.htm">New (Edit) Archive Action Page</MadCap:xref>, <MadCap:xref href="../RecordingPlayback/WorkWithRecordings/ExportRec.htm">Export Recordings</MadCap:xref>, or <MadCap:xref href="../AudioRecordingBasics/CTICore/CTICoreSettingsReference.htm">CTI Core Settings</MadCap:xref>.</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <h2>Location File Mask Structure</h2>
        <p>To construct a location file mask, you must type a path  that incorporates one or more of the variables shown in the next section.</p>
        <p>For example, suppose you need to construct the following path:</p>
        <pre> "C:\recordings\test\device10\10-100-100.wav"</pre>
        <p>These are the variables that would be used and the values they represent:</p>
        <table class="basicTable">
            <col />
            <col />
            <thead>
                <tr>
                    <th>Variable</th>
                    <th>Value</th>
                </tr>
            </thead>
            <tr>
                <td>%P&lt;-1&gt;</td>
                <td>test\device10</td>
            </tr>
            <tr>
                <td>%P&lt;1&gt;</td>
                <td>recordings\test</td>
            </tr>
            <tr>
                <td>%P</td>
                <td>recordings\test\10</td>
            </tr>
            <tr>
                <td>%F</td>
                <td>10-100-100</td>
            </tr>
        </table>
        <h3>Filename Masks in Archive Actions</h3>
        <p>If no mask is specified, <b>Archiver</b> defaults to "%P\%F"; if only "C:\recordings\" or "C:\recordings" is entered, <b>Archiver</b> defaults to "C:\recordings\%P\%F".</p>
        <p>If %P is not used in the file mask, <b>Archiver</b> will determine one on its own using the following process:</p>
        <ul>
            <li class="topic">
                <b>Archiver</b> checks the <b>CTI Core</b> on the server where the file was recorded and obtains the default file mask if possible.</li>
            <li class="topic">
                <b>Archiver</b> tries to match the filename to the default file mask. If it matches, <b>Archiver</b> removes the rest of the path that does not match.</li>
            <li class="topic">If the filename does not match, <b>Archiver</b> removes the drive letter only.</li>
            <li class="topic">The path is put at the end of the archive location, forming the new path.</li>
        </ul>
        <p>The file is then archived, exported, or saved using the generated mask.</p>
        <h3>Filename Masks in Exporting Recordings</h3>
        <p>When exporting recorded interactions, you can create different masks and choose which one to use on a case-by-case basis. You can delete masks that are no longer needed.</p>
        <p>File masks used for exporting recordings use only filename variables, not path variables. You can use an underscore (_) or hyphen (-) between variables to make the mask easier to read. Masks can be used to create any filename that is legal in your system and that matches any file naming strategy used by your organization.</p>
        <p>You cannot edit a filename mask after it has been created, but you can delete filename masks that are no longer needed.</p>
        <p>The following image provides some examples of filename masks. The first column shows the mask name, the middle column shows the path, and the last column shows how the filename would appear.</p>
        <div>
            <img src="../RecordingPlayback/WorkWithRecordings/Images/ExportRec7.png" />
        </div>
        <h2>Location File Mask Variables</h2>
        <dl>
            <dt>%Y</dt>
            <dd>Four-digit year (for example, 2015). This variable can be used with all file masks.</dd>
            <dt>%y</dt>
            <dd>Two-digit year (for example, 15). This variable can be used with all file masks.</dd>
            <dt>%M</dt>
            <dd>Two-digit month (for example, 01-12). This variable can be used with all file masks.</dd>
            <dt>%m</dt>
            <dd>Month name as a three-letter abbreviation (for example, Jan, Feb, Mar, and so forth). This variable can be used with all file masks.</dd>
            <dt>%D</dt>
            <dd>Two-digit day (for example, 01-31). This variable can be used with all file masks.</dd>
            <dt>%d</dt>
            <dd>Day of week name (for example, Monday). This variable can be used with all file masks.</dd>
            <dt>%H</dt>
            <dd>Hour. This variable can be used with all file masks.</dd>
            <dt>%N</dt>
            <dd>Minute. This variable can be used with all file masks.</dd>
            <dt>%S</dt>
            <dd>Second. This variable can be used with all file masks.</dd>
            <dt>%A</dt>
            <dd>Agent Number (ID); also known as Device Alias. This variable can be used with all file masks.</dd>
            <dt>%R</dt>
            <dd>Record ID (Ident). This variable can be used with all file masks.</dd>
            <dt>%U&lt;1&gt; through %U&lt;15&gt;</dt>
            <dd>User fields. This variable can be used with all file masks.</dd>
            <dt>%C</dt>
            <dd>In file masks for archiving and saving, this variable is a Counter. In file masks for exporting calls, this variable is Caller ID.</dd>
            <dt>%c</dt>
            <dd>Dialed Number (DNIS). This variable is used only with file masks for exporting.</dd>
            <dt>%I</dt>
            <dd>Device ID. This variable is used only with file masks for exporting.</dd>
            <dt>%T</dt>
            <dd>Call Duration. This variable is used only with file masks for exporting.</dd>
            <dt>%W</dt>
            <dd>Call Wrap. This variable is used only with file masks for exporting.</dd>
            <dt>%O</dt>
            <dd>Call Direction. This variable is used only with file masks for exporting.</dd>
            <dt>%F</dt>
            <dd>Filename without path. This variable is used only with file masks for archiving and saving.</dd>
            <dt>%P</dt>
            <dd>Path minus root. This variable is used only with file masks for archiving and saving.</dd>
            <dt>%P&lt;-1&gt;</dt>
            <dd>Path minus root remove one bottom directory. This variable is used only with file masks for archiving and saving.</dd>
            <dt>%P&lt;1&gt;</dt>
            <dd>Path minus root remove one top directory. This variable is used only with file masks for archiving and saving.</dd>
        </dl>
        <h2>Related Tasks</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="CreateArchiveActions.htm">Create Archive Actions</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="EditArchiveActions.htm">Edit Archive Actions</MadCap:xref>
                </b>
            </li>
            <li MadCap:conditions="uWFO.Internal">
                <b>
                    <MadCap:xref href="../AudioRecordingBasics/CTICore/CreateCTICores.htm">Create CTI Cores</MadCap:xref>
                </b>
            </li>
            <li MadCap:conditions="uWFO.Internal">
                <b>
                    <MadCap:xref href="../AudioRecordingBasics/CTICore/EditCTICores.htm">Edit CTI Cores</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="../RecordingPlayback/WorkWithRecordings/ExportRec.htm">Export Recordings</MadCap:xref>
                </b>
            </li>
        </ul>
    </body>
</html>