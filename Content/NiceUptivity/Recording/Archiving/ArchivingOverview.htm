﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us">
    <head><title>Archiving Overview</title>
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
        <meta name="description" content="Provides a conceptual overview of archiving recordings in [%=uWFO.NULong%]." />
    </head>
    <body>
        <h1><a name="_Ref405897144"></a><a name="_Ref405897292"></a>Archiving Overview</h1>
        <p>Recordings are typically stored initially on the local hard drives in an <MadCap:variable name="uWFO.NUShort" /> server. However, these drives only have enough space to store audio and video recordings for a short time. Archiving allows you to manage long-term storage of your recordings.</p>
        <h2>Archive Actions</h2>
        <p>Rules governing how recordings are archived are called <i>archive actions</i> and are initially configured during installation. If you need to change these rules, or set up new rules, you can do so later.</p>
        <p>Schedules control when and what calls are recorded (see <MadCap:xref href="../Scheduling/RecordingSchedulesOverview.htm">Recording Schedules Overview</MadCap:xref>). They also define the recording's retention days (that is, how many days a recording is retained on the local system) and the first archive action (that is, what happens to the recording once the retention days have been met).</p>
        <p>Important facts about archive actions include:</p>
        <ul>
            <li class="topic">Archive actions should be configured before you create the schedule in which they are used.</li>
            <li class="topic">If a recording belongs to more than one schedule, the archive action for the schedule with the highest priority takes precedence. If priorities are equal, the earliest-created schedule takes precedence.</li>
            <li class="topic">The default archive action is <span class="fieldValue">Purge</span>, which deletes recordings  from the server once their retention period has been reached.</li>
            <li class="topic">Archive actions can be edited but not deleted. If an archive action is no longer needed, its status can be set to inactive. The status can be changed back to active later if necessary.</li>
            <li class="topic">Archive actions enable you to set different archiving plans for different types of calls. For example, Client A requires calls to be retained for one year, while Client B requires calls to be retained for two years. You can create a one-year archive action and a two-year archive action, and then apply them to separate schedules for each client.</li>
            <li class="topic">Retention days and archive actions are applied when the call is recorded. Changing this value in a schedule only applies to calls made after applying the schedule change, not to calls that have already been recorded.</li>
            <li class="topic">If a call is deleted from the server by an archive action, any QA evaluations performed on that call remain in the database for historical purposes. However, if a call is manually deleted, QA evaluations performed on the call are also deleted.</li>
        </ul>
        <p>You can create a sequence of archive actions if that best meets your organization's needs. For example:</p>
        <ul>
            <li class="topic">The recording schedule sets a value of 30 retention days for audio recordings. Calls are kept on the recording server for that period of time.</li>
            <li class="topic">When the retention period is up, the schedule sets an archive action called ContactCenter. This archive action causes the recordings to be copied to a network share used by the contact center. The ContactCenter archive action sets a value of 90 days until the next archive action, and the recordings will remain on the network share for that period of time. Finally, the next archive action is defined as LongTerm.</li>
            <li class="topic">When the ContactCenter archive action "expires" in 90 days, the LongTerm archive action causes the recordings to be copied to long-term storage on the organization's SAN. The LongTerm archive action sets a value of 730 days until the next archive action, so the recordings will remain on the SAN for two years. Finally, the next archive action is defined as Purge.</li>
            <li class="topic">When the LongTerm archive action "expires" in 730 days, the recordings are deleted.</li>
        </ul>
        <p>It is important to communicate archive action plans to your users so they understand how long recordings are retained by the system. Schedules and archive actions should be audited on a monthly basis to ensure all necessary information is being archived.</p>
        <h2>Archiver</h2>
        <p>
            <MadCap:snippetText src="../../Application/Configuration/ThemeSnippets/ArchiverDesc.flsnp" />
        </p>
        <div style="padding-left: 5px;padding-right: 5px;border-left-style: solid;border-left-width: 2px;border-left-color: #009edb;border-right-style: solid;border-right-width: 2px;border-right-color: #009edb;">
            <p><b>In Version </b><MadCap:variable name="uWFO.Version" style="font-weight: bold;" /><b>:</b> The Archiver  does not archive or purge audio and video files separately  (for media files that contain both audio and video). Archive actions that were set up in previous versions to store or purge audio and video separately will not be performed upon upgrading.</p>
            <p>For example: A version 17.2 <MadCap:variable name="uWFO.NUShort" /> system contains an archive action that follows this sequence:</p>
            <ol class="stepList">
                <li>Stores audio and video recordings to \\exampleServer1\recordings.</li>
                <li>After 60 days, purges video.</li>
                <li>After 120 days, moves audio to long term storage in \\exampleServer2\archiver.</li>
                <li>After 360 days, purges audio from long term storage. </li>
            </ol>
            <p>In this example, when the system is upgraded to <MadCap:variable name="uWFO.Version" />, video will also be archived for 360 days because it cannot be purged separately from the audio.</p>
        </div>
        <div style="padding-left: 5px;padding-right: 5px;border-left-style: solid;border-left-width: 2px;border-left-color: #e7268d;border-right-style: solid;border-right-width: 2px;border-right-color: #e7268d;">
            <p><b>In Version 18.1.1:</b> The Archiver has the ability to store and purge audio and video separately, even from within a WebM file. Earlier version <MadCap:variable name="uWFO.NUShort" /> systems with existing archive actions that stored or purged audio and video files separately can continue doing this upon upgrading to version 18.1.1. </p>
            <p>For example: a rule can be set that video should be purged after 365 days and audio should be purged after 730 days. From within a WebM file, the video will be purged after the 365 days, while the audio remains until its deadline.</p>
        </div>
        <p>Recordings can be archived to attached disks or to Windows Network File Shares (SMB). Archived recordings are still available for playback as long as the local disk is attached, or the network file system is properly configured and available.</p>
        <p>If there are database entries for duplicate or missing files, the Archiver will continue trying to archive these files unsuccessfully (and logging errors) until the invalid database entries are removed.</p>
        <p>Archiver settings are configured during installation of your system, and typically do not need to be changed. If you do change settings for this service, they must be reloaded to take effect. This can be done automatically using the <span class="fieldName">Enable Settings Reload</span> setting, or manually using <span class="interface">Refresh Settings</span> in the <span class="interface">Archiver Console</span>.</p>
        <h2>Archive Media</h2>
        <p><MadCap:variable name="uWFO.NUShort" /> supports four types of archive media:</p>
        <ul>
            <li class="topic"><b>Disk</b> — Use this storage type when you are archiving to a local disk. <MadCap:variable name="uWFO.NUShort" /> must have read/write access to the disk. Disk storage supports the use of location file masks, which allow you to customize UNC paths, file types, and file names used in archiving.</li>
            <li class="topic"><b>SMB</b> — Use this storage type to specify a CIFS/SMB network file storage share for archiving. If <MadCap:variable name="uWFO.NUShort" /> is unable to write to the file share, the system generates an alert (see <MadCap:xref href="../../Application/LoggingAndAlerts/LoggingOverview.htm">Logging and Alerts Overview</MadCap:xref>). SMB storage supports the use of location file masks, which allow you to customize UNC paths, file types, and file names used in archiving.</li>
            <li class="topic"><b>DVD</b> — Use this storage type if you archive to removable media. DVD+R, DVD-R, DVD+RW, and DVD-RW single-layer are supported. Archive actions using the DVD storage type only execute once per day, unless you manually archive recordings to DVD using the <i>Archiver Console</i>. DVD media must be manually removed and new media inserted on a daily basis. DVD storage does not support the use of location file masks.</li>
            <li class="topic"><b>XAM</b> — Use this storage type in conjunction with the fixed-content access method on a Centera content-addressable storage platform. XAM storage does not support the use of location file masks.</li>
        </ul>
        <h2><a name="Related"></a>Related Tasks</h2>
        <ul>
            <li><b><MadCap:xref href="CreateArchiveActions.htm">Create Archive Actions</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="EditArchiveActions.htm">Edit Archive Actions</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="DeleteEmptyDirectories.htm">Delete Empty Archive Directories</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="PurgeFilesManually.htm">Purge Files Manually</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="RunArchiveActions.htm">Run Archive Actions</MadCap:xref></b>
            </li>
        </ul>
        <h2><a name="Related2"></a>Related References</h2>
        <ul>
            <li><b><MadCap:xref href="ArchiveActionListReference.htm">Archive Action List</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="NewArchiveActionReference.htm">New (Edit) Archive Action Page</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="../../Application/Configuration/Services/ArchiverSettingsReference.htm">Archiver Settings</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="LocationFileMaskReference.htm">Location File Masks</MadCap:xref></b>
            </li>
            <li><b><MadCap:xref href="ArchiverConsoleReference.htm">Archiver Console</MadCap:xref></b>
            </li>
        </ul>
    </body>
</html>