﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us">
    <head><title>New (Edit) Archive Action Page</title>
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
        <meta name="description" content="Provides a reference for the fields, settings, and information on the New (Edit) Archive Action page in [%=uWFO.NULong%]." />
    </head>
    <body>
        <h1>New (Edit) Archive Action Page</h1>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Overview</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The <span class="interface">New Archive Action</span> page allows you to configure settings for archive actions.</p>
                <p>This topic explains the settings available when you are creating or editing archive actions.</p>
                <p>For more information, see <MadCap:xref href="ArchivingOverview.htm">Archiving Overview</MadCap:xref>.</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <div class="thumbnailImg">
            <img src="Images/NewArchiveActionRef3.jpg" style="mc-thumbnail: popup;" />
        </div>
        <div class="notification-note">
            <div class="notificationContent">
                <p>This page is called <span class="interface">Edit Archive Action</span> when opened for an existing action.</p>
            </div>
        </div>
        <h2>General Settings</h2>
        <dl>
            <dt class="requiredField">Name</dt>
            <dd>Lists the user-defined name assigned to the archive action when it is created. This name will appear in the list of available archive actions when you configure a schedule, and in the <span class="fieldName">Next Archive Action</span> list on this page.<MadCap:conditionalText MadCap:conditions="uWFO.Internal"> Names created by <MadCap:variable name="uWFO.NUShort" /> installers should be meaningful and describe what the action does (for example, <span class="fieldValue">2 Year Long Term Storage</span>).</MadCap:conditionalText></dd>
            <dt>Storage Type</dt>
            <dd>Select the storage medium used by this archive action from the drop-down list. Possible values are: <span class="fieldValue">Disk</span>, <span class="fieldValue">SMB</span>, <span class="fieldValue">DVD</span>, or <span class="fieldValue">XAM</span>. The default value is <span class="fieldValue">Disk</span>. The next few fields on this page may change depending on your selection. See <a href="#Storage-">Storage Type-Specific Settings</a>.<MadCap:conditionalText MadCap:conditions="uWFO.Internal"> Check this setting before turning an installation over to Optimization. If the action moves files to a different location, this field should be set to <span class="fieldValue">SMB</span> as this allows for use of a service account.</MadCap:conditionalText></dd>
            <dt>Location</dt>
            <dd>Specifies the directory where files will be stored by the archive action.</dd>
            <dt>Archive Restriction</dt>
            <dd>From the drop-down list, select which recording-related files should be governed by this archive action. Possible values are: <span class="fieldValue">Archive Everything</span> or <span class="fieldValue">Archive Audio Only</span><MadCap:conditionalText MadCap:conditions="uWFO.DoNotPRINT">, <span class="fieldValue">Archive Audio &amp; Analytics Only</span> (audio and speech analytics data), or <span class="fieldValue">Archive Audio &amp; Video Only</span> (audio and screen recordings)</MadCap:conditionalText>. The default value is <span class="fieldValue">Archive Everything</span>.</dd>
            <dt>Archive Type</dt>
            <dd>Determines how the files will be moved from the original location to the archive location. Choose one from the drop-down list:
				<ul><li class="topic"><span class="fieldValue">Normal</span> — Files are moved to the archive location and then deleted from the original location. <MadCap:variable name="uWFO.NUShort" /> database call records are updated with the new file location. If you want users to have access to the archived file from <MadCap:variable name="uWFO.NUShort" />, the location must be accessible. If the recording was archived to DVD, <MadCap:variable name="uWFO.NUShort" /> notifies an administrator to load the disk and also notifies the user when the file is available.</li><li class="topic"><span class="fieldValue">Copy</span> — Files are copied to the archive location, and files in the original location remain untouched. If files are later purged from the original location, they must be manually restored for <MadCap:variable name="uWFO.NUShort" /> to access them.</li><li class="topic"><span class="fieldValue">Backup</span> — Files are copied to the archive location and the original files are left alone. Call records are copied to a backup table in the <MadCap:variable name="uWFO.NUShort" /> database and updated with the address of the backup location. When the original file and call record are purged, the backup call record is moved into the live database, making the file retrievable from the backup location.</li></ul></dd>
            <dt>Status</dt>
            <dd>Shows whether the action is <span class="fieldValue">Active</span> (available for use) or <span class="fieldValue">Inactive</span> (unavailable). Does not affect the operation of the action or how it is attached to any recording schedules. Only <span class="fieldValue">Active</span> actions appear in the <span class="fieldName">Next Archive Action</span> list.</dd>
            <dt>Next Archive Action</dt>
            <dd>Select the archive action to follow this one from the drop-down list. Possible values are <span class="fieldValue">Purge</span>, <span class="fieldValue">None</span>, and any archive actions that have been created in your system. The default value, <span class="fieldValue">None</span>, results in no changes to the files after the archive action completes. <span class="fieldValue">Purge </span>results in deletion of the files after the archive action completes.</dd>
            <dt class="requiredField">Days Until Next Archive</dt>
            <dd>Type the number of days between successful execution of the current archive action and the execution of the <span class="fieldName">Next Archive Action</span>. </dd>
            <dt>Use Schedule</dt>
            <dd>Select an option from the drop-down list to limit when the archive action can run. Possible values are: <span class="fieldValue">Yes</span> or <span class="fieldValue">No</span>. <MadCap:conditionalText MadCap:conditions="uWFO.Internal">Check this setting before turning an installation over to Optimization. While the setting can help conserve resources on the server or in connected environments by limiting archiving to non-peak hours, it should typically be set to <span class="fieldValue">No</span>.</MadCap:conditionalText></dd>
            <dt>Start Time</dt>
            <dd>Works in conjunction with the <span class="fieldName">Use Schedule</span> setting. Select a starting time for the period during which the archive action can run. <MadCap:conditionalText MadCap:conditions="uWFO.Internal">Check this setting before turning an installation over to Optimization. Always set a <span class="fieldName">Start Time</span> and <span class="fieldName">End Time</span>. This avoids situations where the customer sets <span class="fieldName">Use Schedule</span> to <span class="fieldValue">Yes</span> and causes the action to run for only one minute per day.</MadCap:conditionalText></dd>
            <dt>End Time</dt>
            <dd>Works in conjunction with the <span class="fieldName">Use Schedule</span> setting. Select an ending time for the period during which the archive action can run. <MadCap:conditionalText MadCap:conditions="uWFO.Internal">Check this setting before turning an installation over to Optimization. Always set a <span class="fieldName">Start Time</span> and <span class="fieldName">End Time</span>. This avoids situations where the customer sets <span class="fieldName">Use Schedule</span> to <span class="fieldValue">Yes</span> and causes the action to run for only one minute per day.</MadCap:conditionalText></dd>
        </dl>
        <h2><a name="Storage-"></a>Storage-Type Specific Settings</h2>
        <h3>Disk Settings</h3>
        <dl>
            <dt class="requiredField">Location</dt>
            <dd>Type a direct file path or a custom location file mask for the archive destination.</dd>
        </dl>
        <h3>SMB Settings</h3>
        <dl>
            <dt>User ID</dt>
            <dd>If the storage location requires credentials, type the username that <MadCap:variable name="uWFO.NUShort" /> will use to access the location.</dd>
            <dt>Password</dt>
            <dd>If the storage location requires credentials, type the password that <MadCap:variable name="uWFO.NUShort" /> will use to access the location.</dd>
            <dt class="requiredField">Location</dt>
            <dd>Type a direct file path or a custom location file mask for the archive destination.<MadCap:conditionalText MadCap:conditions="uWFO.Internal"> Check this setting before turning an installation over to Optimization. The value should include the name of the archive action to help in troubleshooting.</MadCap:conditionalText></dd>
        </dl>
        <h3 MadCap:conditions="uWFO.DoNotPRINT">DVD Settings</h3>
        <dl>
            <dt class="requiredField" MadCap:conditions="uWFO.DoNotPRINT">DVD Drive</dt>
            <dd MadCap:conditions="uWFO.DoNotPRINT">Type the drive letter for the DVD drive. </dd>
            <dt MadCap:conditions="uWFO.DoNotPRINT">Archive Time</dt>
            <dd MadCap:conditions="uWFO.DoNotPRINT">Specify the time each day that <b>Archiver</b> should start creating the DVD archive. Best practice is to schedule DVD archiving during the lowest period of system usage, as creating archives requires high system resources.</dd>
        </dl>
        <h3 MadCap:conditions="uWFO.DoNotPRINT">XAM Settings</h3>
        <dl>
            <dt MadCap:conditions="uWFO.DoNotPRINT">Retention Period (days)</dt>
            <dd MadCap:conditions="uWFO.DoNotPRINT">Type the number of days the Centera server will retain archived files. This setting is used instead of <span class="fieldName">Next Archive Action</span> for this storage type only.</dd>
            <dt MadCap:conditions="uWFO.DoNotPRINT">PEA File</dt>
            <dd MadCap:conditions="uWFO.DoNotPRINT">If security authentication is needed to access the Centera storage server, type the full path name of the Pool Entry Authorization (PEA) file required to access the server.</dd>
            <dt MadCap:conditions="uWFO.DoNotPRINT">Server IP Address</dt>
            <dd MadCap:conditions="uWFO.DoNotPRINT">Type the IP address of the Centera storage server. If there are multiple servers, type the IP addresses separated by a comma.</dd>
        </dl>
        <h2>Information Section</h2>
        <div class="thumbnailImg">
            <img src="Images/NewArchiveActionRef2.jpg" style="mc-thumbnail: popup;" />
        </div>
        <p>This section appears for any archive action that is attached to at least one schedule. The section provides the number of schedules that use the action and the number of recordings currently in queue for this action. It also includes a table with the following information about each schedule to which the action is attached:</p>
        <dl>
            <dt>Name</dt>
            <dd>Displays the name assigned to the schedule when it was created.</dd>
            <dt>Description</dt>
            <dd>Displays the description (if available) assigned to the schedule when it was created.</dd>
            <dt>Owner</dt>
            <dd>Displays the unique database identifier for the person designated as the schedule's owner.</dd>
            <dt>Date Created</dt>
            <dd>Displays the date and time the archive action was associated with the schedule.</dd>
            <p>Clicking on any row in the table takes you to the <b>Edit Schedule</b> page for that schedule.</p>
        </dl>
        <h2>Related Tasks</h2>
        <ul>
            <li class="topic"><b><MadCap:xref href="CreateArchiveActions.htm">Create Archive Actions</MadCap:xref></b> — for more information on tasks that use these settings</li>
            <li class="topic"><b><MadCap:xref href="EditArchiveActions.htm">Edit Archive Actions</MadCap:xref></b> — for more information on tasks that use these settings</li>
        </ul>
    </body>
</html>