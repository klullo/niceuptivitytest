﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:lastBlockDepth="6" MadCap:lastHeight="2261" MadCap:lastWidth="904">
    <head>
        <title>QA Arbitration and Review Process Overview</title>
        <meta name="description" content="Provides a conceptual overview of the QA review process, including acknowledgment and arbitration, in [%=uWFO.QM%]." />
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>QA Arbitration and Review Process Overview</h1>
        <p>
            <MadCap:variable name="uWFO.NUShort" /> provides an interactive process for agents and evaluators to review and agree upon completed evaluations. The process consists of two components: acknowledgment and arbitration. Organizations can decide whether to use acknowledgment only, acknowledgment and arbitration together, or neither component, depending on their needs. Ask your supervisor if you are unsure which process is used in your organization.</p>
        <h2>Acknowledgment Workflow</h2>
        <p>When an acknowledgment-only workflow is chosen, the process looks like this:</p>
        <ul>
            <li class="topic">The evaluator designates the completed evaluation as "Agent Acknowledgment Required."</li>
            <li class="topic">The evaluation is added to the agent's <span class="interface">Assignment Inbox</span> as a <span class="interface">New Assignment</span> and an envelope icon (designating new messages) appears at the top of the agent's <MadCap:variable name="uWFO.dWebPortal" />. Depending on your system configuration, the agent may also be notified via email about the evaluation.</li>
            <li class="topic">After reviewing the completed evaluation, and adding comments if desired, the agent can either acknowledge or question the evaluation. These choices result in a workflow branch.</li>
        </ul>
        <h3>Acknowledged Evaluations</h3>
        <p>If the agent acknowledges the evaluation, it moves to <span class="interface">Reviewed Assignments</span> in the agent's <span class="interface">Assignment Inbox</span> and has a status of <span class="interface">Acknowledged</span> in the list of <span class="interface">QA Acknowledgments</span>. Agents cannot interact with any buttons on an acknowledged evaluation or add additional questions or comments.</p>
        <h3>Questioned Evaluations</h3>
        <p>If the agent questions the evaluation, it moves to <span class="interface">Pending Assignments</span> and has a status of <span class="interface">Question</span> in the list of <span class="interface">QA Acknowledgments</span>. Agents cannot interact with any buttons on a questioned evaluation or add additional questions or comments.</p>
        <p>The evaluator can search for this status and reply to the question. Once the evaluator has replied, the evaluation moves back to the agent's <span class="interface">New Assignments</span>. This Q&amp;A loop may be repeated as necessary until the agent acknowledges the evaluation.</p>
        <h2>Arbitration Workflow</h2>
        <p>When the arbitration workflow is chosen, the process changes to allow for a third-party review of disputed evaluations:</p>
        <ul>
            <li class="topic">The creator of the evaluation form designates that it should be available for use with the arbitration workflow. This workflow is enabled on a form-by-form basis.</li>
            <li class="topic">When performing the evaluation, the evaluator designates one or more evaluators to serve as arbitrators if the evaluation is disputed. There is no limit to the number of arbitrators per evaluation, but only evaluators with appropriate permissions for the agent, group, and form may be selected. If a selected arbitrator changes roles, permissions, or both, disputed evaluations can become stranded. Therefore, <MadCap:variable name="uWFO.Company" /> recommends selecting more than one arbitrator . The evaluator should also designate the completed evaluation as "Agent Acknowledgment Required".</li>
            <li class="topic">When complete, the evaluation is added to the agent's <span class="interface">Assignment Inbox</span> as a <span class="interface">New Assignment</span> and an envelope icon (designating new messages) appears at the top of the agent's <MadCap:variable name="uWFO.dWebPortal" />. Depending on your system configuration, the agent may also be notified via email about the evaluation.</li>
            <li class="topic">After reviewing the completed evaluation, and adding comments if desired, the agent can either acknowledge, question, or dispute the evaluation. These choices result in workflow branches. Acknowledged and questioned evaluations are discussed earlier in this topic.</li>
        </ul>
        <h3>Disputed Evaluations</h3>
        <p>If the agent disputes the evaluation, it moves to <span class="interface">Pending Assignments</span> and has a status of <span class="interface">Dispute</span> in the list of <span class="interface">QA Acknowledgments</span>. Agents cannot interact with any buttons on a disputed evaluation or add additional questions or comments.</p>
        <p>Evaluators and arbitrators can search for this status and see that the evaluation was disputed, but only an arbitrator can respond to the dispute.</p>
        <p>If more than one arbitrator is assigned to an evaluation, the first arbitrator to open and view  the evaluation is designated as the arbitrator for that evaluation, even if they don't edit or rescore it. Any later attempts by other arbitrators to make changes  will result in a message indicating another arbitrator has already rescored the evaluation and that no new changes have been or can be saved.</p>
        <p>Once the arbitrator has replied, the evaluation's status changes to <span class="interface">Completed</span>, it moves back to the agent's <span class="interface">New Assignments</span> and the agent's original three choices (acknowledge, question, and dispute) are once again available. This loop may be repeated as necessary until the agent acknowledges the evaluation.</p>
        <h2>Evaluation Comments </h2>
        <p>Evaluations include <span class="fieldName">Comments</span> fields for  agent, evaluator,  arbitrator, or any of these to use during the review process. All comments are retained on the evaluation throughout this process. These comments also appear when an evaluation is converted to PDF for printing. The <span class="fieldName">Comments</span> fields:</p>
        <ul>
            <li class="topic">Are limited to 1,500 characters (including spaces) per field.</li>
            <li class="topic">Allow percent signs (%), parentheses, hyphens, commas, and periods, but no other special characters.</li>
        </ul>
        <p>If either of these limitations is violated, an error message appears when the form is submitted. </p>
        <h2>Notifications</h2>
        <p>Agents are notified about the status of their evaluations, and can access the evaluations from their <span class="interface">Assignment Inbox</span>. Evaluators and arbitrators receive email notifications when an agent questions or disputes an evaluation.</p>
        <h2>
            <a name="Related">
            </a>Related Tasks</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="AcknowledgeQAEvaluation.htm">Acknowledge QA Evaluations</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="DisputeQAEvaluation.htm">Dispute QA Evaluations</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="QuestionQAEvaluation.htm">Question QA Evaluations</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="RespondtoQuestionedQAEvaluation.htm">Respond to Questioned QA Evaluations</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="RespondtoDisputedQAEvaluation.htm">Respond to Disputed QA Evaluations</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="ViewQAAcknowledgments.htm">View QA Acknowledgments</MadCap:xref>
                </b>
            </li>
        </ul>
        <h2>
            <a name="Related2">
            </a>Related References</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="SearchQAAckReference.htm">Search QA Acknowledgments Page</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="../../Application/WebPortal/AssignmentInbox/AssignmentInboxReference.htm">Assignment Inbox</MadCap:xref>
                </b>
            </li>
        </ul>
    </body>
</html>