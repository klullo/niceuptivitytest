﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:lastBlockDepth="6" MadCap:lastHeight="2498" MadCap:lastWidth="904">
    <head>
        <title>QA Evaluations Overview</title>
        <meta name="description" content="Provides a conceptual overview of how QA evaluations are used in [%=uWFO.QM%]." />
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>
            <a name="_Ref417459405">
            </a>QA Evaluations Overview</h1>
        <p>
            <MadCap:variable name="uWFO.NUShort" /> provides two options for performing QA evaluations.</p>
        <p>Evaluating a <i>call</i> is done from the <span class="interface">Call List</span> or <span class="interface">Recorded Interactions</span> list and allows you to evaluate an agent's performance on a specific interaction.</p>
        <p>Evaluating an <i>agent</i> is done from the <span class="interface">Coaching</span> tab, and allows you to evaluate an agent's handling of alternative channels, like chat or email. You can also use this feature to evaluate employees who do not take calls, such as HR representatives or field technicians. However, the employee must still have the <span class="fieldName">Agent</span> field selected in their user account, and must have a value in the <span class="fieldName">Phones</span> field.</p>
        <p>To work with QA evaluations, you must have the appropriate permissions and access to the <MadCap:variable name="uWFO.dGroups" /> associated with the QA form used for the evaluation. If you need to perform, view, edit, or otherwise work with QA evaluations and are unable to do so, see your <MadCap:variable name="uWFO.NUShort" /> administrator. </p>
        <h2>Performing Evaluations</h2>
        <p>
            <MadCap:variable name="uWFO.NUShort">
            </MadCap:variable> displays the entire selected evaluation form so that you can review it prior to completing it. As you complete the evaluation, you can see a running total of possible points, awarded points, and the percentage score in the bottom right corner of the evaluation.</p>
        <p>A warning icon appears beside responses that were specified as <b>Auto-Fail</b> by the creator of the form.</p>
        <p>You can optionally choose to send completed evaluations  to the agent for acknowledgment. Your organization must grant agents access to the <MadCap:variable name="uWFO.dWebPortal" /> to use this feature. Acknowledgment can be used as an independent feature or as part of the arbitration workflow. The creator of a QA evaluation form can specify that agent acknowledgment is required; however, evaluators can override this setting. You should always require agent acknowledgment when using the arbitration workflow.</p>
        <p>If agent acknowledgment is not required, the completed evaluation is submitted without being sent to the agent for review. The evaluation will not appear in the agent's <span class="interface">Assignment Inbox</span> or in the <span class="interface">Search QA Acknowledgments</span> results; however, it will appear in the <span class="interface">Search QA Evaluations</span> results.</p>
        <h2>In-Progress Evaluations</h2>
        <p>While you are working on an evaluation, <MadCap:variable name="uWFO.NUShort" /> automatically saves it as a draft to protect your work. This auto-save happens:</p>
        <ul>
            <li class="topic">Every five minutes while you are working on the evaluation</li>
            <li class="topic">When you click <span class="interface">Stay</span> in the dialog box that displays when you attempt to navigate away from the evaluation page, or attempt to close your browser, before your work is complete. If you click <span class="interface">Leave</span>, an evaluation will be saved, and be available for editing. However, none of your work will be saved unless more than five minutes has elapsed.</li>
        </ul>
        <p>In either of the latter two situations, you must follow the steps in the <MadCap:xref href="EditQAEvaluation.htm">Edit QA Evaluations</MadCap:xref> task to continue working on the evaluation.</p>
        <p>Because the auto-save happens at five minute intervals, you could lose a small amount of work if your browser, or your entire workstation, unexpectedly shuts down. For example, if your computer shuts down four minutes after an auto-save, you would lose any work done on the evaluation in that four minutes.</p>
        <p>You can manually save an unfinished evaluation to be completed at a later time. Incomplete evaluations are saved with a status of <b>In Progress</b>. You can edit and save an in-progress evaluation multiple times before submitting it as complete. In-progress evaluations cannot be sent to the agent for acknowledgment (for details, see <MadCap:xref href="../ArbitrationAndReview/QAReviewProcessOverview.htm">QA Arbitration and Review Process Overview</MadCap:xref>). Evaluations also do not appear in the <span class="interface">Assignment Inbox</span> or on widgets until they are completed. For related information, see <MadCap:xref href="../../Application/WebPortal/AssignmentInbox/AssignmentInboxOverview.htm">Assignment Inbox Overview</MadCap:xref>.</p>
        <h2>Completed Evaluations</h2>
        <p>Completed evaluations can be sent to agents for their review. They can also be viewed by supervisors from the <span class="interface">Call List</span> or <span class="interface">Recorded Interactions</span> list, or the <span class="interface">Reporting</span> tab. The evaluation displays all possible responses, as well as their associated points. This helps agents and coaches more easily compare possible and awarded scores for each question. </p>
        <p>You can edit or delete a completed evaluation, and you can generate a printable version of standard evaluations (but not calibration or self-evaluations). You can also copy a link that will provide direct evaluation access (that person must have permissions to use <MadCap:variable name="uWFO.NUShort" /> and see the evaluation). </p>
        <p>If a call is deleted from the server, whether manually or via an archive action, any QA evaluations performed on that call remain in the database for historical searching and review.</p>
        <h2>Evaluation Search Criteria</h2>
        <p>When you search for evaluations, you can save criteria selections for future use. These saved criteria sets appear at the top of the <span class="interface">Search QA Evaluations</span> page. Sets can be either public or private.</p>
        <p>Private criteria sets may only be viewed, edited, and deleted by the user who created them. Public sets may be viewed by all users with permissions to search for QA evaluations. However, only the user who created the set can edit or delete it.</p>
        <p>You can designate a set of saved search criteria as your default, and it will load automatically when you click <span class="interface">Search QA Evaluations</span>. When you view QA evaluations from the <span class="interface">Call List</span> or <span class="interface">Recorded Interactions</span> list, <MadCap:variable name="uWFO.NUShort" /> overrides your default criteria to present evaluations associated with a specific call.</p>
        <h2>Evaluation History</h2>
        <p>Evaluation scores can be changed after submission, either through the arbitration process or by direct editing (with appropriate permissions). <MadCap:variable name="uWFO.QM" /> always preserves the original information, including the original evaluator’s name, the date the evaluation was submitted, and the score that was awarded.</p>
        <p>You can search for and report on evaluations based on this original information, the latest information, or a combination of these. If more than one evaluator saves changes to an evaluation, <MadCap:variable name="uWFO.QM" /> tracks only the most recent updates.</p>
        <h2>Related Sub-Themes</h2>
        <ul>
            <li>
                <b>
                    <a href="EvaluateAgent.htm">
                        <MadCap:xref href="Calibration/CalibrationOverview.htm">Calibration Overview</MadCap:xref>
                    </a>
                </b>
            </li>
            <li>
                <b>
                    <a href="EvaluateCall.htm">
                        <MadCap:xref href="SelfEvaluation/SelfEvalOverview.htm">Agent Self-Evaluation Overview</MadCap:xref>
                    </a>
                </b>
            </li>
        </ul>
        <h2>
            <a name="Related">
            </a>Related Tasks</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="EvaluateAgent.htm">Evaluate Agents</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="EvaluateCall.htm">Evaluate Calls</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="ViewCompletedorInProgressEvaluations.htm">View Completed or In-Progress Evaluations</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="CompleteInProgressEvaluation.htm">Complete In-Progress Evaluations</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="EditQAEvaluation.htm">Edit QA Evaluations</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="DeleteQAEvaluation.htm">Delete QA Evaluations</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="SaveSearchCriteriaSet.htm">Save Search Criteria Sets</MadCap:xref>
                </b>
            </li>
        </ul>
        <h2>
            <a name="Related2">
            </a>Related References</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="SearchQAEvaluationReference.htm">Search QA Evaluation Page</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="PerformQAReference.htm"> Perform QA Page</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="AgentFormsListingReference.htm">Agent Forms Listing</MadCap:xref>
                </b>
            </li>
        </ul>
    </body>
</html>