﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:lastBlockDepth="6" MadCap:lastHeight="1822" MadCap:lastWidth="904">
    <head>
        <title>Agent Self-Evaluation Overview</title>
        <meta name="description" content="Provides a conceptual overview of self-evaluations and how they are implemented in [%=uWFO.QM%]." />
        <link href="../../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>Agent Self-Evaluation Overview</h1>
        <p>Self-evaluation is a process that helps managers and supervisors see how agents view their own performance. Agents can easily compare their self-evaluations to those performed by evaluators. This leads to more productive coaching sessions and a sense of agent empowerment, which in turn contribute to higher-quality customer service, reduced agent confusion and frustration, and improved employee morale.</p>
        <h2>Self-Evaluation Workflow</h2>
        <p>
            <MadCap:variable name="uWFO.NUShort" /> supports the agent self-evaluation process by means of specific permissions and a self-evaluation workflow. When your organization chooses to use self-evaluations, the process looks like this:</p>
        <ul>
            <li class="topic">Managers or administrators create one or more QA forms to be used for self evaluations.</li>
            <li class="topic">They notify agents that the form(s) can be used to perform self evaluations.</li>
            <li class="topic">Agents individually score any of their own calls using the designated QA form(s).</li>
        </ul>
        <p>The following considerations apply to self-evaluations in <MadCap:variable name="uWFO.NUShort" />:</p>
        <ul>
            <li class="topic">Managers/administrators must manually notify agents that self-evaluation forms are available. This can be done either outside the <MadCap:variable name="uWFO.NUShort" /> system, or by creating a document with the details, uploading it to the <span class="interface">Content Library</span>, and assigning it to the group associated with the self-evaluation form. The latter approach is a good choice if agents are required to perform self-evaluations.</li>
            <li class="topic">To perform a self-evaluation, the agent must have the <span class="interface">Allow Viewing of User's Own Records</span> and <span class="interface">Allow Performing Self-Evaluations</span> permissions. <span class="interface">Allow Viewing of User's Own Records</span> is included in the <span class="interface">Default Agent</span> role. <span class="interface">Allow Performing Self-Evaluations</span> would need to be added to that role or, alternatively, both permissions would need to be included in a new role. The agent must also belong to the <MadCap:variable name="uWFO.dGroup" /> associated with the self-evaluation form.</li>
            <li class="topic">To view their own completed self-evaluations, agents must also have the <span class="interface">Allow Performing QA Evaluations</span> permission.  As long as the agent's role does not include access permissions for any groups, granting this permission only enables them to search for and view their own evaluations.</li>
            <li class="topic"> The <span class="interface">Allow Performing QA Evaluations</span> permission is also required if agents should be able to save in-progress self-evaluations and finish them later. Agents can access self-evaluations that were saved in-progress from the <span class="interface">Search QA Evaluations</span> page.</li>
            <li class="topic">Completed self-evaluations are not available to the reviewed agent in either the <span class="interface">Assignment Inbox</span> or the <span class="interface">Assignment Inbox</span> widget. Agents with the appropriate permissions can view them either by searching for self-evaluations on the <span class="interface">Search QA Evaluations</span> page or by viewing the <span class="interface">Self-Evaluation Detail Report</span>.</li>
            <li class="topic">Self-evaluation forms cannot be used with either the calibration or arbitration workflows.</li>
            <li class="topic">Self-evaluation is enabled on a per-form basis. If you select <span class="interface">Enable Self-Evaluation</span> when you create a form, all evaluations that use that form are treated as self-evaluations.</li>
            <li class="topic">If you want agents to evaluate themselves using the same questions and responses found on a standard QA evaluation form, you should create two versions of the same form that are identical in content. Include “Self-Evaluation” in the title of one of the forms and select <span class="fieldName">Enable Self-Evaluation</span> for that version of the form. This ensures that the self-evaluations are excluded from standard QA reports and included in self-evaluation reports. It also allows you to use the arbitration and calibration workflows for the standard version of the form. </li>
        </ul>
        <h2>Self-Evaluation Reporting</h2>
        <p>Self-evaluation scores are not included in either standard or ad hoc reports of agent, group, and organizational performance. However, the following reports are available specific to self-evaluations:</p>
        <ul>
            <li class="topic">Self-Evaluation Agent Summary</li>
            <li class="topic">Self-Evaluation Detail</li>
            <li class="topic">Self-Evaluation Group Summary</li>
            <li class="topic">Self-Evaluation Trending</li>
            <li class="topic">Self-Evaluator Comparison</li>
        </ul>
        <p>Parameters specific to self-evaluations can also be used to create ad hoc reports. For additional information, see <MadCap:xref href="../../../Reporting/AdHocReportsOverview.htm">Ad Hoc Reports Overview</MadCap:xref>.</p>
        <h2>Related Tasks</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="../../Forms/CreateQAEvaluationForm.htm">Create QA Evaluation Forms</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="../EvaluateCall.htm">Evaluate Calls</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="../EvaluateAgent.htm">Evaluate Agents</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="../ViewCompletedorInProgressEvaluations.htm">View Completed or In-Progress Evaluations</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="../../ContentLibrary/AddItemtoContentLibrary.htm">Add Items to the Content Library</MadCap:xref>
                </b>
            </li>
        </ul>
        <h2>Related References</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="../PerformQAReference.htm">Perform QA Page</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="../SearchQAEvaluationReference.htm">Search QA Evaluation Page</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="../../Forms/QAFormEditorReference.htm">QA Form Editor</MadCap:xref>
                </b>
            </li>
        </ul>
    </body>
</html>