﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:lastBlockDepth="5" MadCap:lastHeight="3467" MadCap:lastWidth="768">
    <head>
        <title>Achievements Overview</title>
        <meta name="description" content="Provides a conceptual overview of the Achievements feature, which is available in both [%=uWFO.QM%] and [%=uWFO.PerfManLong%]." />
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>Achievements Overview</h1>
        <p>
            <MadCap:variable name="uWFO.NUShort" /> allows you to establish a system for awarding points to agents who have achieved certain pre-defined goals. Identifying and rewarding clearly-defined goals can lead to improved agent performance and productivity, as well as create a positive way to promote competition among agents. </p>
        <p>With <MadCap:variable name="uWFO.NUShort" /> Achievements, you can:</p>
        <ul>
            <li class="topic">Create custom achievements that can be awarded to individual agents or particular <MadCap:variable name="uWFO.dGroups" />.</li>
            <li class="topic">Award points based on an agent's performance on QA evaluations or other goals defined by your organization.</li>
            <li class="topic">Track achievements that have been awarded, along with the total points awarded to each agent.</li>
            <li class="topic">Set a date when achievements can no longer be awarded or when points expire.</li>
            <li class="topic">Automatically notify agents about their own achievements as well as achievements awarded to other agents. </li>
        </ul>
        <h2>Achievement Types</h2>
        <p>Accomplishments in <MadCap:variable name="uWFO.NUShort" /> are called achievement types, and can be:</p>
        <ul>
            <li class="topic">Based on QA evaluation scores. These achievements are awarded automatically by the system as soon as an agent receives the required score on the specified number of QA evaluations.</li>
            <li class="topic">Defined ad hoc according to your organization's needs. Ad hoc achievements are awarded manually when an agent or group accomplishes a pre-defined goal. These goals can be based on any factors or behavior your organization wants to reward, such as survey results, customer feedback, attendance, average handle time, or script adherence.</li>
        </ul>
        <p> An achievement type can have an open-ended award period, or specific start and end dates that indicate when the achievement can be awarded. You cannot add  achievement types with overlapping start and end dates for the same agents, group, or QA form (if applicable). </p>
        <p>Each achievement type must specify the number of points to be awarded and the individual agents or groups eligible to receive the award. Points can be set up to expire in a specified number of days after being awarded or by an expiration date. Expired points are automatically removed from an agent's total point count. </p>
        <p>You can update or edit any of the achievement type fields, even if this achievement has already been awarded to agents. However, when you edit an achievement you must change the start date to be the current date or later, and the end date, if any, to be after the new start date. Edits to an achievement type do not affect achievements that have already been awarded.</p>
        <div class="notification-note">
            <div class="notificationContent">
                <p>Use caution when editing achievement types. If an agent receives the same ad hoc achievement award more than once, it may be confusing if details such as the number of points awarded or the expiration date have changed.</p>
            </div>
        </div>
        <h2>Notification of Achievement Awards</h2>
        <p>When agents are awarded achievement points, either by meeting the scoring criteria for evaluations or by receiving an ad hoc achievement award, they can be notified by any of the following notification methods:</p>
        <ul>
            <li class="topic">Email </li>
            <li class="topic">News widget</li>
            <li class="topic">Achievement widget</li>
        </ul>
        <p>Each achievement type must be set up with one or more of these notification methods. </p>
        <h3>Email Notification</h3>
        <p>When an agent is notified by email that an achievement was awarded, the email includes the following text:</p>
        <p style="text-indent: 0.25in;">
            <b>Email subject:</b> Congratulations on Your Achievement!</p>
        <p style="text-indent: 0.25in;">
            <b>Email body text:</b>
        </p>
        <p style="text-indent: 0.25in;">You have been awarded this achievement: </p>
        <p style="text-indent: 0.25in;">Name: &lt;<i>achievement type name</i>&gt;</p>
        <p style="text-indent: 0.25in;">Points: &lt;<i>number of points awarded for this achievement</i>&gt;</p>
        <p style="text-indent: 0.25in;">Expiration: &lt;<i>expiration date for points</i>&gt;</p>
        <p>To receive an email notification, agents must have an email address specified in their <MadCap:variable name="uWFO.NUShort" /> user account. </p>
        <h3>Widgets</h3>
        <p>The <span class="interface">Achievement</span> widget is automatically available to be added to a user's dashboard on the <span class="interface">Home</span> page. With this widget, agents can view information about achievements awarded to them as well as to other agents. Managers and supervisors, as well as agents, can also use this widget to view a list of high-scoring agents and newly-added achievement types.</p>
        <p>The <span class="interface">News</span> widget displays a posting to all users each time an achievement is awarded. The text of this posting is: &lt;<i>agent name</i>&gt; received a &lt;<i>achievement type name</i>&gt; achievement.</p>
        <p>For more details about the information displayed on these widgets, see <MadCap:xref href="../../Application/Configuration/WFOWidgets/AchievementWidgetReference.htm">Achievement Widget</MadCap:xref> or <MadCap:xref href="../../Application/Configuration/WFOWidgets/NewsWidgetReference.htm">News Widget</MadCap:xref>. </p>
        <h2>Achievement Icons</h2>
        <p>Achievement awards can be represented with an icon that is displayed when the achievement appears in the <span class="interface">Achievement</span> widget. The following five standard icons are preloaded in <MadCap:variable name="uWFO.NUShort" /> and are available to be selected when the achievement type is added or edited:</p>
        <p>
            <img src="Images/AchievementsIcons.png" style="visibility: visible;mso-wrap-style: square;width: 248px;height: 46px;" />
        </p>
        <p>Custom icons can also be loaded to use instead of one of the standard icons. Custom icons have the following format requirements:</p>
        <ul>
            <li class="topic">File size: 105 x 105 pixels </li>
            <li class="topic">File type: PNG </li>
        </ul>
        <h2>Guidelines for Awarding Achievements</h2>
        <p>When awarding achievements based on QA evaluation scores, the following guidelines apply:</p>
        <ul>
            <li class="topic">Achievements cannot be awarded for QA evaluations performed in the past.</li>
            <li class="topic">A QA-based achievement cannot be awarded multiple times for the same evaluation. If an evaluation is re-scored, the achievement is not awarded even if the new score qualifies for an achievement.</li>
            <li class="topic">Changes to a QA evaluation form do not affect achievements that have already been awarded. </li>
        </ul>
        <p>When awarding ad hoc achievements, the following guidelines apply:</p>
        <ul>
            <li class="topic">The same achievement type can be awarded more than once to the same agents or groups, until the achievement end date is reached.</li>
            <li class="topic">If an achievement is awarded to multiple groups and an agent belongs to more than one of those groups, the agent is awarded the achievement only once.</li>
            <li class="topic">You may want to base ad hoc awards on metrics obtained from <MadCap:variable name="uWFO.NUShort" /> reports, such as agent ranking, script adherence, average handle time, or improvement in evaluation scores.</li>
            <li class="topic">You can only award ad hoc achievements to users or groups you have permission to access.</li>
        </ul>
        <h2>User Permissions</h2>
        <p>To perform activities related to setting up an achievement award system, users must have the correct permissions. If you need to perform such activities and are unable to do so, see your <MadCap:variable name="uWFO.NUShort" /> administrator.</p>
        <p>Depending on how your permissions are set, some of the achievement menus may not display. For assistance, contact your <MadCap:variable name="uWFO.NUShort" /> administrator.</p>
        <h2>
            <a name="Related">
            </a>Related Tasks</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="AddAchievementType.htm">Add Achievement Types</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="ViewAchievementTypes.htm">View Achievement Types</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="EditAchievementTypes.htm">Edit Achievement Types</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="AwardAdHocAchievement.htm">Award Ad Hoc Achievements</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="UploadCustom Icon.htm">Upload Custom Icons</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="ViewAwardedAchievements.htm">View Awarded Achievements</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="ViewAgentAchievements.htm">View an Agent's Achievement Details</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="DeleteAgentAchievements.htm">Delete an Agent's Awarded Achievements</MadCap:xref>
                </b>
            </li>
        </ul>
        <h2>
            <a name="Related2">
            </a>Related References</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="AchievementTypesReference.htm">Achievement Types</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="AddAchieveTypesReference.htm">Achievement Type Page</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="AwardedAchievementsReference.htm">Awarded Achievements</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="AchievementDetailsReference.htm">Agent Achievements Details</MadCap:xref>
                </b>
            </li>
        </ul>
    </body>
</html>