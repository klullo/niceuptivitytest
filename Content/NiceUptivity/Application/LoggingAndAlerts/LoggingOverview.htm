﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:lastBlockDepth="5" MadCap:lastHeight="2052" MadCap:lastWidth="1012">
    <head>
        <title>Logging and Alerts Overview</title>
        <meta name="description" content="Provides a conceptual overview of how error logging and alerts work in [%uWFO.NULong%]." />
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>
            <a name="_Ref373915057">
            </a>Logging and Alerts Overview</h1>
        <p>
            <MadCap:variable name="uWFO.NULong" /> can be configured to notify you when issues require your attention. Issues are logged according to severity, and you can choose the level at which notifications are sent.</p>
        <p>For the most part, these settings are configured during initial installation and should only be changed by or under the supervision of <MadCap:variable name="uWFO.NULong" /> Support. However, it's important for you to understand the various logging levels and alerts so you can effectively manage issues that may arise with your system.</p>
        <h2>Application Logs and Alerts</h2>
        <MadCap:snippetBlock src="../Configuration/ThemeSnippets/LoggerDesc.flsnp" />
        <p>The <b>Logger</b> can generate both audible alerts and email notifications. You can set up email subscriptions that send alerts to specific people based on the logging level. For example, you can configure critical and emergency alerts to go to one person or group, and license or archive alerts to go to a different person or group. For additional information, see <MadCap:xref href="UnderstandErrorMessages.htm">Understanding [%=uWFO.NUShort%] Error Messages</MadCap:xref>.</p>
        <p>The <b>Logger</b> also creates files which can be used to see errors, events, and other information associated with different <MadCap:variable name="uWFO.NUShort" /> services. These log files are used by <MadCap:variable name="uWFO.NUShort" /> Support in troubleshooting issues. Some logging information forms the basis of audit reports available through ad hoc reporting.</p>
        <p>During installation, your <MadCap:variable name="uWFO.NUShort" /> team configures which events are logged, and how long log files are retained, based on best practices. Be sure to consider available disk space if you decide to change these settings.</p>
        <h2>
            <a name="Disk">
            </a>Disk Space Notifications</h2>
        <p>Disk space notifications are a special type of alert that can be used to monitor free space on any local or mapped network drives the <b>Archiver</b> service can access. <b>Archiver</b> manages disk usage for original recordings, archives, and SQL databases.</p>
        <p>If a specified drive drops below the notification threshold, <b>Archiver</b> can send a message to any email addresses that have the <span class="interface">Disk Alert</span> notice type selected.</p>
        <p MadCap:conditions="uWFO.Internal">Installation and support engineers should keep the following points in mind when configuring disk space notifications:</p>
        <ul>
            <li class="topic" MadCap:conditions="uWFO.Internal">Notifications should be set to a reasonable threshold, and enough time should be allowed for users to respond to alerts.</li>
            <li class="topic" MadCap:conditions="uWFO.Internal">These are global settings and apply to all servers in a multi-server environment. If you set disk space notifications for a D: drive, and one of the servers in the system has no D: drive, the customer will receive alerts for that server.</li>
            <li class="topic" MadCap:conditions="uWFO.Internal">The <span class="interface">Archiver</span> service must be installed and running on any server that <MadCap:variable name="uWFO.NUShort" /> is to monitor for disk space.</li>
        </ul>
        <h2>SNMP Alerts</h2>
        <p>
            <MadCap:variable name="uWFO.NUShort" /> can send SNMP traps when a log message is generated. SNMP trapping requires use of a Management Information Base (MIB) file that can be loaded in a third-party SNMP management application to define trap types.</p>
        <h2>
            <a name="Related">
            </a>Related Tasks</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="EnablingAudibleAlerts.htm">Enable Audible Alerts</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="DisablingAudibleAlerts.htm">Disable Audible Alerts</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="CreatingEmailAlerts.htm">Create Email Alerts</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="EditingEmailAlerts.htm">Edit Email Alerts</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="DeletingEmailAlerts.htm">Delete Email Alerts</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="CreatingSNMPAlerts.htm">Create SNMP Alerts</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="EditingSNMPAlerts.htm">Edit SNMP Alerts</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="DeletingSNMPAlerts.htm">Delete SNMP Alerts</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="SendingTestAlerts.htm">Send Test Alerts</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="CreatingDiskSpaceNotifications.htm">Create Disk Space Notifications</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="EditingDiskSpaceNotifications.htm">Edit Disk Space Notifications</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="DeletingDiskSpaceNotifications.htm">Delete Disk Space Notifications</MadCap:xref>
                </b>
            </li>
        </ul>
        <h2>
            <a name="Related2">
            </a>Related References</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="NotificationsReference.htm">Notification Settings</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="LoggingTypeReference.htm">Logging Levels</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="../Configuration/Services/LoggerSettingsReference.htm">Logger Service Settings</MadCap:xref>
                </b>
            </li>
        </ul>
    </body>
</html>