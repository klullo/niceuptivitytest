﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:lastBlockDepth="6" MadCap:lastHeight="6416" MadCap:lastWidth="842">
    <head>
        <title>Roles and Permissions Overview</title>
        <meta name="description" content="Provides a conceptual overview of how roles and permissions work in [%=uWFO.NUShort%]." />
        <link href="../../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>
            <a name="_Ref416275383">
            </a>Roles and Permissions Overview</h1>
        <p>Permissions and roles work together to define what users can do in <MadCap:variable name="uWFO.NULong" />. First, you create one or more roles and associate specific permissions with it. Then you assign the role(s) to user(s) so they have access to the information and functionality they need. Key facts about roles include:</p>
        <ul>
            <li class="topic">Roles can be edited, copied, and deleted. Copying roles can help ensure permissions are assigned consistently. For example, you might have two sets of agents who handle calls from different ACD groups, but the agents require the same set of permissions. Copying a role ensures that the agents have exactly the same permissions. You can then associate each role with the appropriate ACD group.</li>
            <li class="topic">Role permissions are cumulative. For example, Role A has Permission 1, and Role B does not have Permission 1. If a user is assigned both Role A and Role B, that user will have Permission 1.</li>
            <li class="topic">
                <MadCap:variable name="uWFO.NUShort" /> permissions do not conflict. </li>
            <li class="topic">
                <MadCap:variable name="uWFO.NUShort" /> allows you to create an unlimited number of roles. Having more roles allows security to be more granular and targeted to the needs of specific users. But more roles can be confusing to administer, and users may not know what roles they need when they request access.</li>
            <li class="topic">You can assign roles to users when you create their accounts and remove roles from users by editing their accounts.
                    </li>
            <li class="topic">You can change role assignments for multiple users at once.</li>
            <li class="topic">The same role can be assigned to multiple users.</li>
            <li class="topic">The same user can be assigned multiple roles.</li>
        </ul>
        <p>At the time of installation, <MadCap:variable name="uWFO.NUShort" /> includes: </p>
        <ul>
            <li class="topic">One default role named <b>DefaultAgent</b>. You cannot delete this role but you can edit its permissions.</li>
            <li class="topic">A <b>Superuser</b> account with permissions to all functionality and data in <MadCap:variable name="uWFO.NULong" />. The superuser access level is not considered a role. You can grant superuser access to individual users, but you should limit the number of superusers for security reasons. Superuser access is not required to administer <MadCap:variable name="uWFO.NUShort" />.</li>
        </ul>
        <h2>
            <a name="Role">
            </a>Associating Roles and Groups</h2>
        <p>The permissions included in a role determine what the user can do. In order to control what the user can access (recordings, agents, and so forth), roles can be associated with <MadCap:variable name="uWFO.dGroups" />, ACD gates, and ACD groups.</p>
        <p>For example, Supervisor A needs to listen to recordings of calls made by agents in ABC group. Supervisor A must be assigned a role with both of these permissions:</p>
        <ul>
            <li class="topic">Allow Viewing All Call Records &amp; QA Evaluations </li>
            <li class="topic">Access to ABC Group</li>
        </ul>
        <h3>associating Roles with <MadCap:variable name="uWFO.dGroups" /></h3>
        <p>When you associate a role with an <MadCap:variable name="uWFO.dGroup" />, users with that role can access all audio recordings associated with that group. Additional permissions are required to view video (that is, screen recordings), QA evaluations, and so forth, for group members.</p>
        <p>Key facts about associating roles with <MadCap:variable name="uWFO.dGroups" /> include:</p>
        <ul>
            <li class="topic">Assigning a user to a role associated with an <MadCap:variable name="uWFO.dGroup" /> does not make the user a member of the group.</li>
            <li class="topic">The same group can be associated with multiple roles.</li>
            <li class="topic">In <span class="interface"><MadCap:variable name="uWFO.CallList" /></span> and <span class="interface"><MadCap:variable name="uWFO.RIList" /></span> list filtering tools for <MadCap:variable name="uWFO.dGroups" />, users will only see those groups associated with their assigned role(s). This is true even if the user is a member of an <MadCap:variable name="uWFO.dGroup" />, but their role is not associated with that group.</li>
        </ul>
        <h3>associating Roles with ACD Groups or gates</h3>
        <p>Most PBX/ACD systems offer one or more means of grouping agents. Depending on the system, terminology may vary: labor groups, hunt groups, skills, gates, and queues are just a few of the naming conventions.</p>
        <p>When you associate a role with an ACD group or gate, users with that role can access all audio recordings associated with that agent grouping. Additional permissions are required to view video (that is, screen recordings), QA evaluations, and so forth, for grouping members.</p>
        <p>Key facts about associating roles with agent groups or gates include:</p>
        <ul>
            <li class="topic">Assigning a user to a role  associated with an ACD group or gate does not make the user  a member of that grouping. These memberships are configured on your PBX/ACD.</li>
            <li class="topic">The same ACD group or gate can be associated with multiple roles.</li>
            <li class="topic">In <span class="interface"><MadCap:variable name="uWFO.CallList" /></span> and <span class="interface"><MadCap:variable name="uWFO.RIList" /></span> list filtering tools, users will see all available groupings listed under the <span class="interface">ACD Group</span> and <span class="interface">ACD Gate</span> menus. However, they will only be able to see and play recordings for a grouping if they have a role with permission to do so.</li>
        </ul>
        <h2>Role Strategy and Design</h2>
        <p>Before creating users, develop a single plan that governs the use of groups and roles. The following two generic plans may help you decide the best approach for your organization. </p>
        <h3>Plan 1: Small Team</h3>
        <p>In this scenario, a company has one location, and 30 agents are divided evenly to work three eight-hour shifts. Each shift has a supervisor who reviews call records and performs quality evaluations.</p>
        <p>The company owner and another employee, the system admin, administer the network and <MadCap:variable name="uWFO.NUShort" />. They can create users, change system settings, and also perform tasks that supervisors do. All calls are for the company's products.</p>
        <p>The company could use the following design.</p>
        <h4>Agent Group and Role</h4>
        <p>All users to be recorded (agents) are placed in an <MadCap:variable name="uWFO.dGroup" /> named Agent and are also assigned a role named Agent. The Agent role has these permissions:</p>
        <ul>
            <li class="topic">Allow Password Changes</li>
            <li class="topic">Allow Viewing of QA Evaluations</li>
            <li class="topic">Allow Viewing Call Reports</li>
            <li class="topic">Allow Viewing QA Reports</li>
            <li class="topic">Allow Viewing of User's Own Records</li>
        </ul>
        <div class="notification-note">
            <div class="notificationContent">
                <p>These are the same permissions included in the <span class="interface">Default Agent</span> role, which could be used instead.</p>
            </div>
        </div>
        <h4>Supervisor Group and Role</h4>
        <p>All supervisors are placed in an <MadCap:variable name="uWFO.dGroup" /> named Supervisor, and are also assigned a role named Supervisor. The Supervisor role has these permissions:</p>
        <ul>
            <li class="topic">Allow Viewing of QA Evaluations</li>
            <li class="topic">Allow QA Form Administration</li>
            <li class="topic">Allow Content Library Management</li>
            <li class="topic">Allow Editing of Completed QA Evaluations</li>
            <li class="topic">Allow Performing QA Evaluations</li>
            <li class="topic">Allow Viewing Call Reports</li>
            <li class="topic">Allow Viewing QA Reports</li>
            <li class="topic">Allow Viewing of User's Own Records</li>
            <li class="topic">Allow Viewing All Call Records &amp; QA Evaluations</li>
        </ul>
        <p>The Supervisor role is also associated with the Agent group.</p>
        <h4>Administrator Role</h4>
        <p>A role named Administrator is assigned directly to the company owner and the system administrator, and includes these permissions:</p>
        <ul>
            <li class="topic">Allow User Administration</li>
            <li class="topic">Allow Password Changes</li>
            <li class="topic">Allow System Configuration</li>
            <li class="topic">Allow Scheduling</li>
            <li class="topic">Allow Group Administration</li>
            <li class="topic">Allow Viewing of QA Evaluations</li>
            <li class="topic">Allow QA Form Administration</li>
            <li class="topic">Allow Content Library Management</li>
            <li class="topic">Allow Editing of Completed QA Evaluations</li>
            <li class="topic">Allow Performing QA Evaluations</li>
            <li class="topic">Allow Viewing Call Reports</li>
            <li class="topic">Allow Viewing QA Reports</li>
            <li class="topic">Allow Viewing of User's Own Records</li>
            <li class="topic">Allow Viewing All Call Records &amp; QA Evaluations</li>
        </ul>
        <h3>Plan 2: Multiple Teams</h3>
        <p>In this scenario, the company has three locations and 120 agents who work a variety of shifts. All agents answer calls for the company's products. Some English-speaking agents answer calls for a new product, Product X. Another group answers Spanish callers.</p>
        <p>The company owner, the IT manager, and the contact center manager administer the network and <MadCap:variable name="uWFO.NUShort" />. They can create users, change system settings, and also perform tasks that supervisors do.</p>
        <p>This more complex organization could use the following design.</p>
        <h4>Agent Group and Role</h4>
        <p>All agents who take English calls are placed in an <MadCap:variable name="uWFO.dGroup" /> named Agent, and are also assigned a role named Agent. The Agent role has these permissions:</p>
        <ul>
            <li class="topic">Allow Password Changes</li>
            <li class="topic">Allow Viewing of QA Evaluations</li>
            <li class="topic">Allow Viewing Call Reports</li>
            <li class="topic">Allow Viewing QA Reports</li>
            <li class="topic">Allow Viewing of User's Own Records</li>
        </ul>
        <div class="notification-note">
            <div class="notificationContent">
                <p>These are the same permissions included in the <span class="interface">Default Agent</span> role, which could be used instead.</p>
            </div>
        </div>
        <h4>Supervisor Group and Role</h4>
        <p>All supervisors of English-language agents are placed in an <MadCap:variable name="uWFO.dGroup" /> named Supervisor, and are also assigned a role named Supervisor. The Supervisor role has these permissions:</p>
        <ul>
            <li class="topic">Allow Viewing of QA Evaluations</li>
            <li class="topic">Allow QA Form Administration</li>
            <li class="topic">Allow Content Library Management</li>
            <li class="topic">Allow Editing of Completed QA Evaluations</li>
            <li class="topic">Allow Performing QA Evaluations</li>
            <li class="topic">Allow Viewing Call Reports</li>
            <li class="topic">Allow Viewing QA Reports</li>
            <li class="topic">Allow Viewing of User's Own Records</li>
            <li class="topic">Allow Viewing All Call Records &amp; QA Evaluations</li>
        </ul>
        <p>The Supervisor role is also associated with the Agent group.</p>
        <h4>Spanish Agent Group and Role</h4>
        <p>Only agents who answer Spanish callers are assigned to this group. They are also assigned a role named Spanish Agent. This role has the same permissions as the Agent role, but is also associated with the ACD Gate that routes Spanish callers.</p>
        <h4>Spanish Supervisor Group and Role</h4>
        <p>Only supervisors for the agents who answer Spanish callers are assigned to this group. They are also assigned a role named Spanish Supervisor. This role has the same permissions as the Supervisor role, but is associated with the Spanish Agent <MadCap:variable name="uWFO.dGroup" />.</p>
        <h4>Product X Group and Role</h4>
        <p>Agents who take calls for Product X are assigned to this group. They are assigned the Agent role and also assigned a second role named Product X. This role is associated with the ACD Gate that routes Product X callers. This design allows for the correct association between agents and calls, and enables reporting specific to Product X interactions. Any supervisor can evaluate these calls, so the Supervisor role is associated with the Product X group. There is no need for a Product X Supervisor group or role.</p>
        <h4>Administrator Role</h4>
        <p>A role named Administrator can be assigned directly to the company owner, IT administrator, and the contact center manager. The Administrator role includes these permissions:</p>
        <ul>
            <li class="topic">Allow User Administration</li>
            <li class="topic">Allow Password Changes</li>
            <li class="topic">Allow System Configuration</li>
            <li class="topic">Allow Scheduling</li>
            <li class="topic">Allow Group Administration</li>
            <li class="topic">Allow Viewing of QA Evaluations</li>
            <li class="topic">Allow QA Form Administration</li>
            <li class="topic">Allow Content Library Management</li>
            <li class="topic">Allow Editing of Completed QA Evaluations</li>
            <li class="topic">Allow Performing QA Evaluations</li>
            <li class="topic">Allow Viewing Call Reports</li>
            <li class="topic">Allow Viewing QA Reports</li>
            <li class="topic">Allow Viewing of User's Own Records</li>
            <li class="topic">Allow Viewing All Call Records &amp; QA Evaluations</li>
        </ul>
        <h3>Specialized roles</h3>
        <p>Roles can also be created to fine-tune permissions for features like quality management and reporting. These roles can then be assigned to users as needed. This allows you to maintain base roles for the majority of users while providing flexibility for custom roles.</p>
        <p>For example, suppose only three of your contact center supervisors should be able to create new QA forms. You could create a specialized role named QA Form Designer. Then you would assign the <span class="interface">Allow QA Form Administration</span> permission only to that role, and that role only to the relevant users.</p>
        <p>As another example, some organizations have only a few employees who create and manage custom reports. In this scenario, you could create a role named Reporting Admin. Then you would assign the <span class="interface">Allow WFO Ad Hoc Reporting</span> and <span class="interface">Allow Report Subscriptions</span> permissions only to that role, and that role only to relevant users.</p>
        <p>This approach is often used for granting permissions related to applications like <MadCap:variable name="uWFO.Speech" /> and <MadCap:variable name="uWFO.Survey" />.</p>
        <h2>AD Group Role Synchronization</h2>
        <p>To simplify administration, users may be placed in one or more Active Directory (AD) groups that have access to <MadCap:variable name="uWFO.NUShort" />. You can then relate <MadCap:variable name="uWFO.NUShort" /> roles to AD group names, and user roles, permissions, and associated <MadCap:variable name="uWFO.dGroups" /> are synchronized at each login based on the user’s AD group membership. This feature is called <b>AD Group Role Synch</b>.</p>
        <p>
            <span class="interface">AD Group Role Synch</span> is typically configured during your initial system installation. Because AD groups may change after installation, you may need to edit or configure these settings from time to time. For more information on these settings, see <MadCap:xref href="../Configuration/SecuritySettingsReference.htm">Security Settings</MadCap:xref>.</p>
        <h2>
            <a name="Related">
            </a>Related Tasks</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="CreateRoles.htm">Create Roles</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="EditRoles.htm">Edit Roles</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="CopyRoles.htm">Copy Roles</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="EditRolesForMultUsers.htm">Edit Role Assignments for Multiple Users</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="DeleteRoles.htm">Delete Roles</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="AddGroupforADSynch.htm">Add Active Directory (AD) Groups for Role Synchronization</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="EditGroupforADSynch.htm">Edit Active Directory (AD) Groups for Role Synchronization</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="RemoveGroupfromADSynch.htm">Remove Active Directory (AD) Groups from Role Synchronization</MadCap:xref>
                </b>
            </li>
        </ul>
        <h2>
            <a name="Related2">
            </a>Related References</h2>
        <ul>
            <li>
                <b>
                    <MadCap:xref href="NewRoleReference.htm">New Role Page</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="AssignRolesReference.htm">Assign Users to Roles Page</MadCap:xref>
                </b>
            </li>
            <li>
                <b>
                    <MadCap:xref href="RolesReference.htm">Roles List</MadCap:xref>
                </b>
            </li>
        </ul>
    </body>
</html>