﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="7" MadCap:lastHeight="1435" MadCap:lastWidth="1149">
    <head><title>[%=uWFO.NULong%] Release Reference</title>
        <link href="../../Resources/Stylesheets/master.css" rel="stylesheet" type="text/css" />
        <meta name="description" content="Provides a high-level reference to the [%=uWFO.NULong%] documentation workflow for a new release." />
    </head>
    <body>
        <h1><MadCap:variable name="uWFO.NULong" /> Release Reference</h1>
        <MadCap:dropDown MadCap:conditions="DocOnly.DocScreenOnly">
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Overview</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The <MadCap:variable name="uWFO.NULong" /> documentation team creates and manages deliverables for three separate audiences: external customers, internal customers, and partners. These deliverables are produced according to the release schedule for the <MadCap:variable name="uWFO.NULong" /> product.</p>
                <p> This topic provides a high-level reference to the documentation workflow for each release. </p>
                <p>For more information, see <MadCap:xref href="TechDocsOverview.htm">[%=uWFO.NULong%] Documentation Overview</MadCap:xref>.</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <div class="notification-note">
            <div class="notificationContent">
                <p>This topic is meant to provide guidelines for the structure of release-related documentation work, and a reference for the work each release requires. Different writers approach their work in different ways, and there is no one "right" way to prepare for a release.</p>
            </div>
        </div>
        <p><b>Committed Features</b> —Work for a release officially begins when Development has provided Product with a list of features they are committing to have ready for a numbered release (for example, 17.3, 18.1, and so forth). You should be able to view this list in JIRA.</p>
        <div class="notification-note">
            <div class="notificationContent">
                <p> Between releases, you should try to be as involved in sprint planning and backlog grooming as possible. You can provide valuable insights to both Product and Development due to your holistic view of <MadCap:variable name="uWFO.NULong" />. Involvement in pre-release meetings also helps you be more prepared for how your workload will be affected by a release. </p>
            </div>
        </div>
        <p><b>Work Estimation</b> — Review the committed features and create work items in the JIRA Documentation project as appropriate. Some features may not require documentation work (for example, a feature that affects how one service talks to another). Identify the appropriate subject matter expert(s) (SME) to provide technical review for each item. In some cases this will be the product manager/owner. In others, it might be a developer, a QA analyst, or someone from Support. Finally, each documentation work item should be t-shirt sized based on the complexity of the feature and the number of topics or guides affected. This is important because the documentation effort required by a feature often bears little resemblance to the development work required. Using development's t-shirt sizing to plan your work is inefficient at best and incorrect at worst. The following table shows the scale currently in use.</p>
        <table class="NiceUpTable">
            <col />
            <col />
            <thead>
                <tr>
                    <th>T-Shirt Size</th>
                    <th>Length of Time to Complete Documentation</th>
                </tr>
            </thead>
            <tr>
                <td>XS</td>
                <td>One day or less</td>
            </tr>
            <tr>
                <td>S</td>
                <td>More than one and less than three days</td>
            </tr>
            <tr>
                <td>M</td>
                <td>More than three and less than five days</td>
            </tr>
            <tr>
                <td>L</td>
                <td>More than five and less than 10 days</td>
            </tr>
            <tr>
                <td>XL</td>
                <td>Ten or more days</td>
            </tr>
        </table>
        <p><b>Work Assignment</b> — A breakdown of work assignments is typically done by the most experienced team member, and then discussed and mutually agreed-upon by all team members. Having specific assignments helps minimize source control conflict caused by two writers working on the same content.</p>
        <p><b>Branch Project</b> — For details on this task, see <MadCap:xref href="../FlareProject/Tasks/BranchProjectForNewRelease.htm">Branch Project for New Release</MadCap:xref>. </p>
        <p><b>Complete Documentation</b> — Complete the documentation work for each feature. How your gather information for your content varies depending on the feature. Typically, you will talk to the product manager and the developer(s) who worked on the feature. If possible, attend a demo of the feature (request one if no demo is scheduled). Keep the <MadCap:xref href="DocVMReference.htm">[%=uWFO.NULong%] VM for Technical Documentation</MadCap:xref> updated at least weekly during active documentation for a release, as you need to be able to use new features yourself in order to document them.</p>
        <p>Documentation is not complete until it has undergone peer and SME review...unless the release timeline is truncated and the help must be published early. In this scenario, make sure your SME reviewer(s) know that if they don't provide feedback by your deadline, the content will be published as is and edited in a later update.</p>
        <p><b>Documentation Not in Flare</b> — For more information, see <MadCap:xref href="DocsNotInFlareReference.htm">Documentation Produced in Microsoft Word</MadCap:xref>. Several items that are produced for every release are described in this topic, including the <MadCap:xref href="DocsNotInFlareReference.htm#CumulativeChange">Cumulative Change Guide</MadCap:xref>, the <MadCap:xref href="DocsNotInFlareReference.htm#Upgrade">Upgrade Guide</MadCap:xref>, and the <MadCap:xref href="DocsNotInFlareReference.htm#ServerSupport">Server Support Matrix</MadCap:xref>. If any new recording integrations are part of the release, the <MadCap:xref href="DocsNotInFlareReference.htm#Integrations">Integrations Matrix</MadCap:xref> must also be updated and published.</p>
        <p><b>Build and Publish Internal Help</b> — For more details on this task, see <MadCap:xref href="../FlareProject/Tasks/BuildAndPublishInternalHelp.htm">Build and Publish Internal Help</MadCap:xref>. You should always build the Internal help site first, and the goal is to publish this site when the release is dev complete and passed to the Installation team for review and deployment testing.
        <div class="notification-note"><div class="notificationContent"><p>Pay special attention to steps 2-8 of this task as they have added importance when you are publishing the first online help for a new release.</p></div></div></p>
        <p><b>Build and Publish Customer Help</b> — For more details on this task, see <MadCap:xref href="../FlareProject/Tasks/BuildAndPublishCustomerHelp.htm">Build and Publish Customer Help</MadCap:xref>. The goal is to publish this site by no later than the publicized release date for the software version.
</p>
    </body>
</html>